unit getVersion;

interface
uses
    windows,Forms,SysUtils;

function GetApplicationVersionString(pApplication:TApplication) : string;

implementation

function GetApplicationVersionString(pApplication:TApplication) : string;
var
    VerSize : Dword;
    Zero : THandle;
    PBlock : Pointer;
    PS : Pointer;
    Size : Uint;
begin
    result := '0.0.0.0';
    VerSize := GetFileVersionInfoSize(PChar(pApplication.ExeName),Zero);
    if VerSize = 0 then exit;
    getMem(PBlock,VerSize);
    GetFileVersionInfo(pChar(pApplication.ExeName),0,VerSize,PBlock);
    VerQueryValue(PBlock,pChar('\\StringFileInfo\\040C04E4\\FileVersion'),PS,Size);
    Result := StrPas(PS);
end;

end.
