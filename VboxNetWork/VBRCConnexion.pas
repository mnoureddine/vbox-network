unit VBRCConnexion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls,VBRCDonnees,VBUtiles;

type
  TFormVBRCConnexion = class(TForm)
    Label1: TLabel;
    EditServeur: TEdit;
    EditUtilisateur: TEdit;
    EditMotDePasse: TEdit;
    ButtonAnnuler: TButton;
    ButtonConnxion: TButton;
    SpinEditPort: TSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CheckBoxEnregistrerMotDePasse: TCheckBox;
    CheckBoxJournaliser: TCheckBox;
    Label6: TLabel;
    ComboBoxLangue: TComboBox;
    procedure FormCreate(Sender: TObject);
  private

    procedure SetEnregistrerMotDePasse(const Value: boolean);
    procedure SetMotDePasse(const Value: string);
    procedure SetPort(const Value: Integer);
    procedure SetServeur(const Value: string);
    procedure SetUtilisateur(const Value: string);
    function GetEnregistrerMotDePasse: boolean;
    function GetMotDePasse: string;
    function GetPort: Integer;
    function GetServeur: string;
    function GetUtilisateur: string;
    procedure SetJournaliser(const Value: boolean);
    function GetJournaliser: boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
    property Serveur : string read GetServeur write SetServeur;
    property Port : Integer read GetPort write SetPort;
    property Utilisateur : string read GetUtilisateur write SetUtilisateur;
    property MotDePasse : string read GetMotDePasse write SetMotDePasse;
    property EnregistrerMotDePasse: boolean read GetEnregistrerMotDePasse write SetEnregistrerMotDePasse;
    property Journaliser : boolean read GetJournaliser write SetJournaliser;
    procedure Charger;
    procedure Enregsitrer;
    function Executer : boolean;
  end;



implementation

{$R *.dfm}

{ TForm1 }

procedure TFormVBRCConnexion.Charger;
begin
  Serveur := Donnees.Parametres.VirtualBox.Serveur;
  Port := Donnees.Parametres.VirtualBox.Port;
  Utilisateur := Donnees.Parametres.VirtualBox.Utilisateur;
  MotDePasse := Donnees.Parametres.VirtualBox.MotDePasse;
  EnregistrerMotDePasse := Donnees.Parametres.VirtualBox.EnregistreMotDePasse;
  Journaliser := Donnees.Parametres.VirtualBox.Journaliser;

end;

procedure TFormVBRCConnexion.Enregsitrer;
begin
  Donnees.Parametres.VirtualBox.Serveur := Serveur;
  Donnees.Parametres.VirtualBox.Port := Port;
  Donnees.Parametres.VirtualBox.Utilisateur := Utilisateur;
  Donnees.Parametres.VirtualBox.EnregistreMotDePasse :=EnregistrerMotDePasse;
  if Donnees.Parametres.VirtualBox.EnregistreMotDePasse then
    Donnees.Parametres.VirtualBox.MotDePasse := MotDePasse
  else
    Donnees.Parametres.VirtualBox.MotDePasse := '';

  Donnees.EnregistrerParametres;
  Donnees.Parametres.VirtualBox.Journaliser := Journaliser;
  Donnees.Parametres.General.Language := ComboBoxLangue.Text;

end;

function TFormVBRCConnexion.Executer: boolean;
begin
  Charger;
  Result := ShowModal = mrOk;
  if Result then
    Enregsitrer;
end;

function TFormVBRCConnexion.GetEnregistrerMotDePasse: boolean;
begin
  Result := CheckBoxEnregistrerMotDePasse.Checked;
end;

function TFormVBRCConnexion.GetJournaliser: boolean;
begin
  Result := CheckBoxJournaliser.Checked;
end;

function TFormVBRCConnexion.GetMotDePasse: string;
begin
  Result := EditMotDePasse.Text;
end;

function TFormVBRCConnexion.GetPort: Integer;
begin


  Result := SpinEditPort.Value;
end;

function TFormVBRCConnexion.GetServeur: string;
begin
  Result := EditServeur.Text;
end;

function TFormVBRCConnexion.GetUtilisateur: string;
begin
  Result := EditUtilisateur.Text;
end;

procedure TFormVBRCConnexion.SetEnregistrerMotDePasse(const Value: boolean);
begin
  CheckBoxEnregistrerMotDePasse.Checked := Value;
end;

procedure TFormVBRCConnexion.SetJournaliser(const Value: boolean);
begin
  CheckBoxJournaliser.Checked := Value;
end;

procedure TFormVBRCConnexion.SetMotDePasse(const Value: string);
begin
  EditMotDePasse.Text:= Value;
end;

procedure TFormVBRCConnexion.SetPort(const Value: Integer);
begin
  SpinEditPort.Value := Value;
end;

procedure TFormVBRCConnexion.SetServeur(const Value: string);
begin
  EditServeur.Text := Value;
end;

procedure TFormVBRCConnexion.SetUtilisateur(const Value: string);
begin
  EditUtilisateur.Text := Value;
end;

procedure TFormVBRCConnexion.FormCreate(Sender: TObject);
var
  sr:TSearchRec;
begin
  TranslateAll(Self,LanguageFile);
  ComboBoxLangue.Items.Clear;
  if FindFirst(LanguagesDir+'\*.lng',faAnyFile,sr) = 0 then
  begin
    repeat
      ComboBoxLangue.Items.Add(sr.Name)
    until FindNext(sr)<>0;
    FindClose(sr);
  end;

  ComboBoxLangue.Text := Donnees.Parametres.General.Language;
  
end;

end.
