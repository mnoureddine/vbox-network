unit VBRSMachinePropriete;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls,VBNVirtualBox;

type
  TFormVMachinePropriete = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ButtonAnnuler: TButton;
    ButtonOK: TButton;
    Panel3: TPanel;
    ImageVBoxMachine: TImage;
    LabelMachine: TLabel;
    LabelUUID: TLabel;
    CheckBoxLancerAuDemarrage: TCheckBox;
  private
    { Déclarations privées }
    Machine : TVBNVirtualBoxMachine;
    function GetLancerAuDemarrage: boolean;
    procedure SetLancerAuDemarrage(const Value: boolean);
    procedure SetNom(const Value: string);
    procedure SetUUID(const Value: string);
  public
    { Déclarations publiques }
    property Nom : string write SetNom;
    property UUID : string write SetUUID;
    property LancerAuDemarrage : boolean read GetLancerAuDemarrage write SetLancerAuDemarrage;
    procedure Charger;
    function executer(pMachine : TVBNVirtualBoxMachine):boolean;
    procedure Enregistrer;
  end;



implementation

{$R *.dfm}

{ TFormVMachinePropriete }

procedure TFormVMachinePropriete.Charger;
begin
  Nom := Machine.Nom;
  UUID := Machine.UUID;
  LancerAuDemarrage := Machine.LancerAuDemarrage;
end;

procedure TFormVMachinePropriete.Enregistrer;
begin
  Machine.LancerAuDemarrage :=  LancerAuDemarrage;

end;

function TFormVMachinePropriete.executer(
  pMachine: TVBNVirtualBoxMachine): boolean;
begin
  Machine := pMachine;
  Charger;
  Result := ShowModal = mrOK;
  if Result then
    Enregistrer;

end;


function TFormVMachinePropriete.GetLancerAuDemarrage: boolean;
begin
  Result := CheckBoxLancerAuDemarrage.Checked;
end;



procedure TFormVMachinePropriete.SetLancerAuDemarrage(const Value: boolean);
begin
  CheckBoxLancerAuDemarrage.Checked := Value;
end;


procedure TFormVMachinePropriete.SetNom(const Value: string);
begin
  LabelMachine.Caption := 'Nom : '+Value;
end;

procedure TFormVMachinePropriete.SetUUID(const Value: string);
begin
  LabelUUID.Caption := 'UUID : '+Value;
end;

end.
