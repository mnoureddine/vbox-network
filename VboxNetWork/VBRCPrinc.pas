unit VBRCPrinc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ShellApi, StdCtrls, ExtCtrls,FileCtrl,IniFiles, ComCtrls,ShellCommande,VirtualBoxClient,VBUtiles,VBRCDonnees,
  Menus, ActnList, ImgList,PNGImage,VBRCConnexion,GetVersion, XPMan;

type

  TFormPrinc = class(TForm)
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    TreeViewMachines: TTreeView;
    ButtonMachineLancer: TButton;
    ButtonMachineReset: TButton;
    ButtonMachineEteindre: TButton;
    ButtonMachineExtinction: TButton;
    ButtonMachineMiseEnVeille: TButton;
    ButtonMachineSauver: TButton;
    ImageMachine: TImage;
    Panel1: TPanel;
    MemoMachine: TMemo;
    MainMenu1: TMainMenu;
    Machines1: TMenuItem;
    Actualiser1: TMenuItem;
    ActionList1: TActionList;
    ActionActualiserMachines: TAction;
    ImageList1: TImageList;
    Panel2: TPanel;
    ButtonMachinePause: TButton;
    TimerActualiser: TTimer;
    ButtonMachineReprendre: TButton;
    ImageVBoxMachine: TImage;
    Panel3: TPanel;
    LabelMachineNom: TLabel;
    LabelMachineUUID: TLabel;
    Connexion1: TMenuItem;
    ActionConnexion: TAction;
    LabelJournal: TLabel;
    ListBoxJournal: TListBox;
    ButtonActualiserImage: TButton;
    ActionActualiserImage: TAction;
    StatusBar1: TStatusBar;
    ActionQuitter: TAction;
    N1: TMenuItem;
    Quitter1: TMenuItem;
    Panel4: TPanel;
    Splitter1: TSplitter;
    ButtonControler: TButton;
    Label1: TLabel;
    Splitter2: TSplitter;
    Panel5: TPanel;
    XPManifest1: TXPManifest;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionActualiserMachinesExecute(Sender: TObject);
    procedure TreeViewMachinesClick(Sender: TObject);
    procedure ButtonMachineLancerClick(Sender: TObject);
    procedure ButtonMachineResetClick(Sender: TObject);
    procedure ButtonMachineEteindreClick(Sender: TObject);
    procedure ButtonMachineExtinctionClick(Sender: TObject);
    procedure ButtonMachineMiseEnVeilleClick(Sender: TObject);
    procedure ButtonMachineSauverClick(Sender: TObject);
    procedure ButtonMachinePauseClick(Sender: TObject);
    procedure TimerActualiserTimer(Sender: TObject);
    procedure ButtonMachineReprendreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActionConnexionExecute(Sender: TObject);
    procedure ActionActualiserImageExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActionQuitterExecute(Sender: TObject);
    procedure ButtonControlerClick(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    procedure ActualiserMachines;
    procedure ActualiserMachinesActive;
    procedure ActualiserMachineImage;
    procedure ActualiserClient;
    procedure VirtualBoxConnected(Sender : TObject);
    procedure VirtualBoxDisconnected(Sender : TObject);
    procedure VirtualBoxEcrireMessage(Sender : TObject;pMessage:String);

  end;







var
  FormPrinc: TFormPrinc;
  FirstTimeShow : boolean;

implementation

{$R *.dfm}

procedure TFormPrinc.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : integer;
begin
  Donnees.VirtualBox.Deconnexion;
  Donnees.EnregistrerParametres;
  
end;

procedure TFormPrinc.ActualiserMachines;
var
  i,j : integer;
  NodeMachine : TTreeNode;
  Trouver : boolean;
  Suppression : boolean;
begin
  ActualiserClient;
  Donnees.VirtualBox.Actualiser;
//  TreeViewMachines.Items.Clear;

  Repeat
    Suppression := false;
    for i := 0 to TreeViewMachines.Items.Count -1 do
    begin
      if Donnees.VirtualBox.Machines.ItemNom[TreeViewMachines.Items[i].Text] = nil then
      begin
        TreeViewMachines.Items[i].Delete;
        Suppression := true;
        break;
      end;
    end;
  until not Suppression;


  for i := 0 to Donnees.VirtualBox.Machines.Count -1 do
  begin
    Trouver := false;
    for j := 0 to TreeViewMachines.Items.Count -1 do
      if SameText(TreeViewMachines.Items[j].Text,Donnees.VirtualBox.Machines[i].Nom) then
      begin
        trouver := true;
        break;
      end;

    if Not Trouver then
    begin
      NodeMachine := TreeViewMachines.Items.AddChild(nil,Donnees.VirtualBox.Machines[i].Nom);
      NodeMachine.Data := Donnees.VirtualBox.Machines[i];
    end;
  end;
end;

procedure TFormPrinc.ActionActualiserMachinesExecute(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
  begin
    ActualiserMachines;
    ActualiserMachinesActive;
    ActualiserMachineImage;
  end;
end;

procedure TFormPrinc.TreeViewMachinesClick(Sender: TObject);
var
  MachineSelect : boolean;
  Machine : TVirtualBoxClientMachine;
begin
  MachineSelect := TreeViewMachines.Selected <> nil;
  ButtonMachineLancer.Enabled := MachineSelect;
  ButtonMachineReset.Enabled := MachineSelect;
  ButtonMachineEteindre.Enabled := MachineSelect;
  ButtonMachineExtinction.Enabled := MachineSelect;
  ButtonMachineMiseEnVeille.Enabled := MachineSelect;
  ButtonMachineSauver.Enabled := MachineSelect;
  if MachineSelect then
  begin
    Machine := TVirtualBoxClientMachine(TreeViewMachines.Selected.Data);
    LabelMachineNom.Caption := 'Machine : ' +Machine.Nom;
    LabelMachineUUID.Caption := 'UUID : ' +Machine.UUID;

    MemoMachine.Lines.SetText(Pchar(Machine.LireInfo));
    ImageMachine.Picture := nil;
  end;

end;

procedure TFormPrinc.ButtonMachineLancerClick(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
    if TreeViewMachines.Selected <> nil then
      TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).Lancer;


end;

procedure TFormPrinc.ButtonMachineResetClick(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
    if TreeViewMachines.Selected <> nil then
      TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).Reset;

end;

procedure TFormPrinc.ButtonMachineEteindreClick(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
    if TreeViewMachines.Selected <> nil then
      TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).Eteindre;

end;

procedure TFormPrinc.ButtonMachineExtinctionClick(Sender: TObject);
begin
    if Donnees.VirtualBox.Connected then
      if TreeViewMachines.Selected <> nil then
        TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).Extinction;

end;

procedure TFormPrinc.ButtonMachineMiseEnVeilleClick(Sender: TObject);
begin
    if Donnees.VirtualBox.Connected then
      if TreeViewMachines.Selected <> nil then
        TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).MiseEnVeille;

end;

procedure TFormPrinc.ButtonMachineSauverClick(Sender: TObject);
begin
    if Donnees.VirtualBox.Connected then
      if TreeViewMachines.Selected <> nil then
        TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).Sauver;

end;


procedure TFormPrinc.ButtonMachinePauseClick(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
    if TreeViewMachines.Selected <> nil then
     TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).Pause;
end;

procedure TFormPrinc.TimerActualiserTimer(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
  begin
    ActualiserMachines;
    ActualiserMachinesActive;
    ActualiserMachineImage;
  end;
end;

procedure TFormPrinc.ButtonMachineReprendreClick(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
     if TreeViewMachines.Selected <> nil then
      TVirtualBoxClientMachine(TreeViewMachines.Selected.Data).Reprendre;
end;

procedure TFormPrinc.ActualiserMachinesActive;
var
  i : integer;
  Machine : TVirtualBoxClientMachine;
begin
  Donnees.VirtualBox.MachinesActives;
  for  i := 0 to TreeViewMachines.Items.Count -1 do
  begin
    if TVirtualBoxClientMachine(TreeViewMachines.Items[i].Data).Active then
    begin
      TreeViewMachines.Items[i].ImageIndex := 1;
      TreeViewMachines.Items[i].StateIndex := 1;
      TreeViewMachines.Items[i].SelectedIndex := 1;
    end
    else
    begin
      TreeViewMachines.Items[i].ImageIndex := 0;
      TreeViewMachines.Items[i].StateIndex := 0;
      TreeViewMachines.Items[i].SelectedIndex := 0;
    end;
  end;

end;

procedure TFormPrinc.FormCreate(Sender: TObject);
begin
  TranslateAll(Self,LanguageFile);
  Donnees.VirtualBox.OnConnected := VirtualBoxConnected;
  Donnees.VirtualBox.OnDisconnected := VirtualBoxDisconnected;
  Donnees.VirtualBox.OnEcrireMessage := VirtualBoxEcrireMessage;
  ActualiserClient;
  FirstTimeShow := false;
end;

procedure TFormPrinc.VirtualBoxConnected(Sender: TObject);
begin

end;

procedure TFormPrinc.VirtualBoxDisconnected(Sender: TObject);
begin
  ActualiserClient;
  TimerActualiser.Enabled := false;
  TreeViewMachines.Items.Clear;

end;

procedure TFormPrinc.ActionConnexionExecute(Sender: TObject);


begin
  if not Donnees.VirtualBox.Connected then
  begin
    with TFormVBRCConnexion.Create(self) do
    begin


        if Executer then
        begin

          Donnees.VirtualBox.Host := Serveur;
          Donnees.VirtualBox.Port :=  Port;
          Donnees.VirtualBox.Utilisateur := Utilisateur;
          Donnees.VirtualBox.MotDePasse := MotDePasse;
          LabelJournal.Visible := Journaliser;
          ListBoxJournal.Visible := Journaliser;
          Donnees.VirtualBox.Connexion;
          if Donnees.VirtualBox.Connected then
          begin
            ActualiserClient;
            ActualiserMachines;
            ActualiserMachinesActive;
            ActualiserMachineImage;
            TimerActualiser.Enabled := true;
          end;

        end;

        free;
    end;
  end
  else
  begin
    Donnees.VirtualBox.Deconnexion;
    
  end;


end;

procedure TFormPrinc.VirtualBoxEcrireMessage(Sender: TObject;
  pMessage: String);
begin
  ListBoxJournal.Items.Insert(0,pMessage);
end;

procedure TFormPrinc.ActualiserMachineImage;
var
  Machine : TVirtualBoxClientMachine;
  FichierPNG : string;
  ImagePNG : TPNGObject;
begin
  if TreeViewMachines.Selected <> nil then
  begin
    Machine := TVirtualBoxClientMachine(TreeViewMachines.Selected.Data);
    if Machine.Active then
    begin
      FichierPNG := Donnees.VirtualBox.DossierTravail+'\machinedistante.png';
      Machine.ImprimeEcran(FichierPNG);
      if FileExists(FichierPNG) then
      begin
        ImageMachine.Picture.LoadFromFile(FichierPNG);
      end;
    end;

  end;

end;

procedure TFormPrinc.ActionActualiserImageExecute(Sender: TObject);
begin
  if Donnees.VirtualBox.Connected then
    ActualiserMachineImage;
end;

procedure TFormPrinc.ActualiserClient;
var
  Etat : string;
begin
  if Donnees.VirtualBox.Connected then
  begin
    Etat := 'Client connect� � '+Donnees.VirtualBox.Host
        +':'+IntToStr(+Donnees.VirtualBox.Port);
    ActionConnexion.Caption := 'D�connecter';
  end
  else
  begin
    Etat := 'Client d�connect�.';
    ActionConnexion.Caption := 'Connecter';
  end;

  Caption := Application.Title +' '+GetApplicationVersionString(Application) +' : '+Etat;
  StatusBar1.SimpleText := Etat;
end;

procedure TFormPrinc.FormShow(Sender: TObject);
begin
  ActualiserClient;
  if not FirstTimeShow then
  begin
    ActionConnexionExecute(sender);
    FirstTimeShow := true;

  end;

end;

procedure TFormPrinc.ActionQuitterExecute(Sender: TObject);
begin
  Close;
end;

procedure TFormPrinc.ButtonControlerClick(Sender: TObject);
var
  Machine : TVirtualBoxClientMachine;
begin
  if Donnees.VirtualBox.Connected then
    if TreeViewMachines.Selected <> nil then
    begin
      Machine := TVirtualBoxClientMachine(TreeViewMachines.Selected.Data);
      if not Machine.Active then
      begin
        Machine.Lancer;
      end;
      if Machine.ActiverBureau then
      begin
        Machine.PrendreConrole;

      end;
    end;

end;

end.

