
unit HTMLFoundry;

interface
uses
    classes,SysUtils,StrUtils;
type
  THTMLFoundryKey = class(TObject)
  private
    FName: string;
    FReplace: string;
    procedure SetName(const Value: string);
    procedure SetReplace(const Value: string);
  protected

  public
    property Name : string read FName write SetName;
    property Replace : string read FReplace write SetReplace;
    constructor Create;
    destructor Destroy; override;
  published

  end;

  THTMLFoundryKeyList = class(TList)
  private
    function Get(Index: Integer): THTMLFoundryKey;
    procedure Put(Index: Integer; Item: THTMLFoundryKey);
    function GetItemsNames(Index: string): THTMLFoundryKey;
    procedure PutItemsNames(Index: string; const Value: THTMLFoundryKey);
    function GetNames(Index: string): string;
    procedure PutNames(Index: string; const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    function Add(Item: THTMLFoundryKey): Integer;overload;
    function Add : THTMLFoundryKey;overload;
    function Add(pName:string;pReplace : string) : THTMLFoundryKey;overload;
    function Extract(Item: THTMLFoundryKey): THTMLFoundryKey;
    function First: THTMLFoundryKey;
    function IndexOf(Item: THTMLFoundryKey): Integer;
    procedure Insert(Index: Integer; Item: THTMLFoundryKey);
    function Last: THTMLFoundryKey;
    procedure Delete(Index: Integer);
    function Remove(Item: THTMLFoundryKey): Integer;
    property Items[Index: Integer]: THTMLFoundryKey read Get write Put; default;
    property ItemsNames[Index: string]: THTMLFoundryKey read GetItemsNames write PutItemsNames;
    property Names[Index: string]: string read GetNames write PutNames;
    procedure Clear; override;
  end;

    THTMLFoundry = class(TObject)
    private
    FFileInput: TFilename;
    FFileOuput: TFilename;
    FKeys: THTMLFoundryKeyList;
    FHTML: TStrings;
    FCarKey: char;
    procedure SetFileInput(const Value: TFilename);
    procedure SetFileOuput(const Value: TFilename);
    procedure SetKeys(const Value: THTMLFoundryKeyList);
    procedure SetHTML(const Value: TStrings);
    procedure SetCarKey(const Value: char);

    protected


    public
      procedure Perform;
      procedure PerformFromFiles;
      procedure LoadKeysFromFile;
      procedure LoadKeys;
      constructor Create;
      destructor Destroy;
    published
      property FileInput : TFilename read FFileInput write SetFileInput;
      property FileOuput : TFilename read FFileOuput write SetFileOuput;
      property CarKey : char read FCarKey write SetCarKey;
      property Keys : THTMLFoundryKeyList read FKeys write SetKeys;
      property HTML : TStrings read FHTML write SetHTML;
    end;


implementation

{ THTMLFoundryKey }

constructor THTMLFoundryKey.Create;
begin
  inherited;

end;

destructor THTMLFoundryKey.Destroy;
begin

  inherited;
end;

procedure THTMLFoundryKey.SetName(const Value: string);
begin
  FName := Value;
end;

procedure THTMLFoundryKey.SetReplace(const Value: string);
begin
    if StrScan(pChar(Value),'%') <> nil then
        FReplace := Value;
end;


{ THTMLFoundryKeyList }

function THTMLFoundryKeyList.Add(Item: THTMLFoundryKey): Integer;
begin
  result := inherited Add(Item);
end;

function THTMLFoundryKeyList.Add: THTMLFoundryKey;
begin
  Result := THTMLFoundryKey.Create;
  Add(Result);
end;

function THTMLFoundryKeyList.Add(pName, pReplace: string): THTMLFoundryKey;
begin
    Result := GetItemsNames(pName);
    if Result = nil then
        result := Add;
    result.FName :=pName;
    result.FReplace := pReplace;


end;

procedure THTMLFoundryKeyList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;

constructor THTMLFoundryKeyList.Create;
begin
  inherited;

end;

procedure THTMLFoundryKeyList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor THTMLFoundryKeyList.Destroy;
begin
  inherited;
end;

function THTMLFoundryKeyList.Extract(Item: THTMLFoundryKey): THTMLFoundryKey;
begin
  result := inherited Extract(Item);
end;

function THTMLFoundryKeyList.First: THTMLFoundryKey;
begin
  Result := inherited First;
end;

function THTMLFoundryKeyList.Get(Index: Integer): THTMLFoundryKey;
begin
  Result := inherited Get(Index);
end;

function THTMLFoundryKeyList.GetItemsNames(
  Index: string): THTMLFoundryKey;
var
    i : integer;
begin
    result := nil;
    for i := 0 to Count -1 do
    begin
        if StrIComp(pchar(Index),pchar(Items[i].FName))=0 then
        begin
            Result := Items[i];
            exit;
        end;

    end;
end;

function THTMLFoundryKeyList.GetNames(Index: string): string;
var
    HTMLFoundryKey : THTMLFoundryKey;
begin
    result := ' ERROR ';
    HTMLFoundryKey := GetItemsNames(Index);
    if HTMLFoundryKey <> nil then
        result := HTMLFoundryKey.FReplace;

end;

function THTMLFoundryKeyList.IndexOf(Item: THTMLFoundryKey): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure THTMLFoundryKeyList.Insert(Index: Integer; Item: THTMLFoundryKey);
begin
  inherited Insert(Index,Item);
end;

function THTMLFoundryKeyList.Last: THTMLFoundryKey;
begin
  Result := inherited Last;
end;

procedure THTMLFoundryKeyList.Put(Index: Integer; Item: THTMLFoundryKey);
begin
  inherited Put(Index,Item);

end;

procedure THTMLFoundryKeyList.PutItemsNames(Index: string;
  const Value: THTMLFoundryKey);
var
    HTMLFoundryKey : THTMLFoundryKey;
begin
    HTMLFoundryKey := GetItemsNames(Index);
    if HTMLFoundryKey <> nil then
        Items[IndexOf(HTMLFoundryKey)] := Value;
end;

procedure THTMLFoundryKeyList.PutNames(Index: string;
  const Value: string);
var
    HTMLFoundryKey : THTMLFoundryKey;
begin
    HTMLFoundryKey := GetItemsNames(Index);
    if HTMLFoundryKey <> nil then
        HTMLFoundryKey.FReplace := value;

end;

function THTMLFoundryKeyList.Remove(Item: THTMLFoundryKey): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;

{ THTMLFoundry }

constructor THTMLFoundry.Create;
begin
    FKeys := THTMLFoundryKeyList.Create;
    FHTML := TStringList.Create;
    FCarKey := '�';
end;

destructor THTMLFoundry.Destroy;
begin
    FKeys.Free;
    FHTML.Free;
end;

procedure THTMLFoundry.LoadKeys;
type
    TSearchStep = (SSKeyStart,SSKeyEnd);
var
    TextIn : pchar;
    Key : string;
    SearchStep : TSearchStep;
begin
    TextIn := HTML.GetText;
    SearchStep := SSKeyStart;
    while TextIn^ <> #0 do
    begin
        case SearchStep of
        SSKeyStart :
        begin
            if TextIn^ = FCarKey then
            begin
                Key := '';
                SearchStep := SSKeyEnd;
            end;
        end;
        SSKeyEnd :
        begin
            if TextIn^ = FCarKey then
            begin
                Keys.Add(Key,'');
                Key := '';
                SearchStep := SSKeyStart;
            end
            else
            begin
                Key := Key + TextIn^;
            end;
        end;
        end;
        inc(TextIn);
    end;

end;


procedure THTMLFoundry.LoadKeysFromFile;
begin
    HTML.LoadFromFile(FFileInput);
    LoadKeys;
end;

procedure THTMLFoundry.Perform;
type
    TSearchStep = (SSKeyStart,SSKeyEnd);
var
    TextIn : pchar;
    Key : string;
    TextOut : string;
    SearchStep : TSearchStep;
begin
    TextIn := HTML.GetText;
    TextOut := '';
    SearchStep := SSKeyStart;
    while TextIn^ <> #0 do
    begin
        case SearchStep of
        SSKeyStart :
        begin
            if TextIn^ = FCarKey then
            begin
                Key := '';
                SearchStep := SSKeyEnd;
            end
            else
            begin
                TextOut := TextOut + TextIn^;
            end;
        end;
        SSKeyEnd :
        begin
            if TextIn^ = FCarKey then
            begin
                TextOut := TextOut + Keys.Names[Key];
                Key := '';
                SearchStep := SSKeyStart;
            end
            else
            begin
                Key := Key + TextIn^;
            end;
        end;
        end;
        inc(TextIn);
    end;
    HTML.SetText(pchar(TextOut));
end;

procedure THTMLFoundry.PerformFromFiles;
begin
    HTML.LoadFromFile(FFileInput);
    Perform;
    HTML.SaveToFile(FFileOuput);
end;

procedure THTMLFoundry.SetCarKey(const Value: char);
begin
  FCarKey := Value;
end;

procedure THTMLFoundry.SetFileInput(const Value: TFilename);
begin
  FFileInput := Value;
end;

procedure THTMLFoundry.SetFileOuput(const Value: TFilename);
begin
  FFileOuput := Value;
end;

procedure THTMLFoundry.SetHTML(const Value: TStrings);
begin
  FHTML := Value;
end;

procedure THTMLFoundry.SetKeys(const Value: THTMLFoundryKeyList);
begin
  Keys := Value;
end;

end.
