unit Princ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ShellApi, StdCtrls, ExtCtrls,FileCtrl,IniFiles, ComCtrls,ShellCommande,VirtualBox,VBUtiles,VBDonnees,
  Menus, ActnList, ImgList,PNGImage;

type

  TFormPrinc = class(TForm)
    LabeledEditDossierTravail: TLabeledEdit;
    MemoResultat: TMemo;
    ButtonPacourir: TButton;
    Label2: TLabel;
    ButtonExecuter: TButton;
    OpenDialog1: TOpenDialog;
    ComboBoxLignesCommande: TComboBox;
    Label3: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TreeViewMachines: TTreeView;
    ButtonMachineLancer: TButton;
    ButtonMachineReset: TButton;
    ButtonMachineEteindre: TButton;
    ButtonMachineExtinction: TButton;
    ButtonMachineMiseEnVeille: TButton;
    ButtonMachineSauver: TButton;
    StaticTextMachine: TStaticText;
    ImageMachine: TImage;
    Panel1: TPanel;
    MemoMachine: TMemo;
    MainMenu1: TMainMenu;
    Machines1: TMenuItem;
    Actualiser1: TMenuItem;
    ActionList1: TActionList;
    ActionActualiserMachines: TAction;
    ImageList1: TImageList;
    Panel2: TPanel;
    ButtonMachinePause: TButton;
    Timer1: TTimer;
    procedure ButtonPacourirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonExecuterClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionActualiserMachinesExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeViewMachinesClick(Sender: TObject);
    procedure ButtonMachineLancerClick(Sender: TObject);
    procedure ButtonMachineResetClick(Sender: TObject);
    procedure ButtonMachineEteindreClick(Sender: TObject);
    procedure ButtonMachineExtinctionClick(Sender: TObject);
    procedure ButtonMachineMiseEnVeilleClick(Sender: TObject);
    procedure ButtonMachineSauverClick(Sender: TObject);
    procedure ButtonMachinePauseClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }


    procedure AjouterParamtres(pParametres : String);
    procedure ActualiserMachines;
    procedure VitualBoxShellExecute(Sender : TObject;Resultat : string);
  end;







var
  FormPrinc: TFormPrinc;

implementation

{$R *.dfm}



procedure TFormPrinc.ButtonPacourirClick(Sender: TObject);
var
  Dossier : String;
begin
    Dossier :=LabeledEditDossierTravail.Text;
  if(SelectDirectory('Dossier de travail','',Dossier)) then
    LabeledEditDossierTravail.Text := Dossier;

end;

procedure TFormPrinc.FormCreate(Sender: TObject);
begin

  Donnees.VirtualBox.OnShellExecute := VitualBoxShellExecute;



end;

procedure TFormPrinc.ButtonExecuterClick(Sender: TObject);
begin
  Donnees.ShellCommande.DossierTravail := LabeledEditDossierTravail.Text;
  Donnees.ShellCommande.LigneCommande := ComboBoxLignesCommande.Text;
  Donnees.ShellCommande.Execute;
  MemoResultat.Lines.SetText(Donnees.ShellCommande.Resultat.GetText);
  AjouterParamtres(ComboBoxLignesCommande.Text);
end;


procedure TFormPrinc.AjouterParamtres(pParametres: String);
var
   i : Integer;
begin
  for i := 0 to ComboBoxLignesCommande.Items.Count - 1 do
    if SameText(pParametres,ComboBoxLignesCommande.Items[i])then
      exit;

  ComboBoxLignesCommande.Items.insert(0,pParametres);
end;






procedure TFormPrinc.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : integer;
begin
  with Donnees.Parametres.ShellCommand do
  begin
    DossierDeTravail := LabeledEditDossierTravail.Text;
    Parametre := ComboBoxLignesCommande.Text;
    Historique.AddStrings(ComboBoxLignesCommande.Items);
  end;

  Donnees.EnregistrerParametres;
end;

procedure TFormPrinc.ActualiserMachines;
var
  i : integer;
  NodeMachine : TTreeNode;
begin
  Donnees.VirtualBox.Actualiser;
  TreeViewMachines.Items.Clear;
  for i := 0 to Donnees.VirtualBox.Machines.Count -1 do
  begin
    NodeMachine := TreeViewMachines.Items.AddChild(nil,Donnees.VirtualBox.Machines[i].Nom);
    NodeMachine.Data := Donnees.VirtualBox.Machines[i];
    
  end;
end;

procedure TFormPrinc.ActionActualiserMachinesExecute(Sender: TObject);
begin
  ActualiserMachines;
end;

procedure TFormPrinc.FormShow(Sender: TObject);
begin
  ActualiserMachines;
end;

procedure TFormPrinc.TreeViewMachinesClick(Sender: TObject);
var

  MachineSelect : boolean;
  Machine : TVirtualBoxMachine;
begin
  MachineSelect := TreeViewMachines.Selected <> nil;
  ButtonMachineLancer.Enabled := MachineSelect;
  ButtonMachineReset.Enabled := MachineSelect;
  ButtonMachineEteindre.Enabled := MachineSelect;
  ButtonMachineExtinction.Enabled := MachineSelect;
  ButtonMachineMiseEnVeille.Enabled := MachineSelect;
  ButtonMachineSauver.Enabled := MachineSelect;
  if MachineSelect then
  begin
    Machine := TVirtualBoxMachine(TreeViewMachines.Selected.Data);
    StaticTextMachine.Caption :=  Machine.Nom +' ' +Machine.UUID;
    MemoMachine.Lines.SetText(Pchar(Machine.LireInfo));
    ImageMachine.Picture := nil;
  end;
  Timer1Timer(sender);
end;

procedure TFormPrinc.ButtonMachineLancerClick(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVirtualBoxMachine(TreeViewMachines.Selected.Data).Lancer;


end;

procedure TFormPrinc.ButtonMachineResetClick(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVirtualBoxMachine(TreeViewMachines.Selected.Data).Reset;

end;

procedure TFormPrinc.ButtonMachineEteindreClick(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVirtualBoxMachine(TreeViewMachines.Selected.Data).Eteindre;

end;

procedure TFormPrinc.ButtonMachineExtinctionClick(Sender: TObject);
begin
    if TreeViewMachines.Selected <> nil then
      TVirtualBoxMachine(TreeViewMachines.Selected.Data).Extinction;

end;

procedure TFormPrinc.ButtonMachineMiseEnVeilleClick(Sender: TObject);
begin
    if TreeViewMachines.Selected <> nil then
      TVirtualBoxMachine(TreeViewMachines.Selected.Data).MiseEnVeille;

end;

procedure TFormPrinc.ButtonMachineSauverClick(Sender: TObject);
begin
    if TreeViewMachines.Selected <> nil then
      TVirtualBoxMachine(TreeViewMachines.Selected.Data).Sauver;

end;

procedure TFormPrinc.VitualBoxShellExecute(Sender: TObject;
  Resultat: string);
begin
  MemoResultat.Lines.SetText(PChar(Resultat));
end;

procedure TFormPrinc.ButtonMachinePauseClick(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVirtualBoxMachine(TreeViewMachines.Selected.Data).Pause;
end;

procedure TFormPrinc.Timer1Timer(Sender: TObject);
var
  i : integer;
  Machine : TVirtualBoxMachine;
  FichierPNG : string;
  ImagePNG : TPNGObject;
begin
  Donnees.VirtualBox.MachinesActives;
  for  i := 0 to TreeViewMachines.Items.Count -1 do
  begin
    if TVirtualBoxMachine(TreeViewMachines.Items[i].Data).Active then
    begin
      TreeViewMachines.Items[i].ImageIndex := 1;
      TreeViewMachines.Items[i].StateIndex := 1;
      TreeViewMachines.Items[i].SelectedIndex := 1;
    end
    else
    begin
      TreeViewMachines.Items[i].ImageIndex := 0;
      TreeViewMachines.Items[i].StateIndex := 0;
      TreeViewMachines.Items[i].SelectedIndex := 0;

    end;
  end;

  if TreeViewMachines.Selected <> nil then
  begin
    Machine := TVirtualBoxMachine(TreeViewMachines.Selected.Data);
    if Machine.Active then
    begin
      FichierPNG := Donnees.VirtualBox.DossierTravail+'\machine.png';
      Machine.ImrpimeEcran(FichierPNG);
      if FileExists(FichierPNG) then
      begin
  //      ImagePNG := TPNGObject.Create;
//        ImagePNG.LoadFromFile(FichierPNG);

        ImageMachine.Picture.LoadFromFile(FichierPNG);
      end;
    end;

  end;


end;

end.

