program VboxNetworkServer;

uses
  Forms,
  VBRSPrinc in 'VBRSPrinc.pas' {FormPrinc},
  ShellCommande in 'ShellCommande.pas',
  VBNVirtualBox in 'VBNVirtualBox.pas',
  VBUtiles in 'VBUtiles.pas',
  VBRSParametres in 'VBRSParametres.pas' {FormVBParametres},
  VBRSDonnees in 'VBRSDonnees.pas' {Donnees: TDataModule},
  VBRSMachinePropriete in 'VBRSMachinePropriete.pas' {FormVMachinePropriete},
  VBRSTexte in 'VBRSTexte.pas',
  VirtualBox_TLB in 'VirtualBoxClient\VirtualBox_TLB.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'VboxNetwork';
  Application.CreateForm(TDonnees, Donnees);
  Application.CreateForm(TFormPrinc, FormPrinc);
  Application.Run;
end.
