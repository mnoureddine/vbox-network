unit VirtualBoxClient;

interface

uses
  Classes,ShellCommande,Registry,Windows,idTCPClient,VirtualBox,ShellAPI;


type
  TVirtualBoxClient = class;

  TVirtualBoxClientMachine = class(TObject)
  private
    FNom: string;
    FUUID: string;
    FEtat: TVBMachineEtat;
    VirtualBoxClient : TVirtualBoxClient;
    FInfo: string;
    FActive: boolean;
    FVRDEPort: Integer;
    FVRDEActif: boolean;
    procedure SetNom(const Value: string);
    procedure SetUUID(const Value: string);
    procedure SetEtat(const Value: TVBMachineEtat);
    procedure SetInfo(const Value: string);
    procedure SetVRDEPort(const Value: Integer);
    procedure SetVRDEActif(const Value: boolean);
  protected

  public
    property Nom : string read FNom write SetNom;
    property UUID : string read FUUID write SetUUID;
    property Etat : TVBMachineEtat read FEtat write SetEtat;
    property Info : string read FInfo write SetInfo;
    property Active : boolean read FActive;
    property VRDEPort : Integer read FVRDEPort write SetVRDEPort;
    property VRDEActif : boolean read FVRDEActif write SetVRDEActif;
    procedure Lancer;
    function LireInfo : string;
    procedure Reprendre;
    procedure Pause;
    procedure Reset;
    procedure Eteindre;
    procedure Sauver;
    procedure Extinction;
    procedure MiseEnVeille;
    procedure ImprimeEcran(FichierPNG : string);
    function ActiverBureau : boolean;
    procedure PrendreConrole;
    constructor Create(pVirtualBoxClient:TVirtualBoxClient; pNom : string;pUUID : string);
    destructor Destroy; override;
  published

  end;

  TVirtualBoxClientMachineList = class(TList)
  private
    VirtualBoxClient:TVirtualBoxClient;
    function Get(Index: Integer): TVirtualBoxClientMachine;
    procedure Put(Index: Integer; Item: TVirtualBoxClientMachine);
    function GetItemNom(pNom: string): TVirtualBoxClientMachine;
    function GetItemUUID(pUUID: string): TVirtualBoxClientMachine;
    procedure SetItemNom(pNom: string; const Value: TVirtualBoxClientMachine);
    procedure SetItemUUID(pUUID: string; const Value: TVirtualBoxClientMachine);
  public
    constructor Create(pVirtualBoxClient:TVirtualBoxClient);
    destructor Destroy; override;
    function Add(Item: TVirtualBoxClientMachine): Integer;overload;
    function Add(pNom : string;pUUID : string ): TVirtualBoxClientMachine;overload;
    function Extract(Item: TVirtualBoxClientMachine): TVirtualBoxClientMachine;
    function First: TVirtualBoxClientMachine;
    function IndexOf(Item: TVirtualBoxClientMachine): Integer;
    procedure Insert(Index: Integer; Item: TVirtualBoxClientMachine);
    function Last: TVirtualBoxClientMachine;
    procedure Delete(Index: Integer);
    function Remove(Item: TVirtualBoxClientMachine): Integer;
    property Items[Index: Integer]: TVirtualBoxClientMachine read Get write Put; default;
    property ItemNom[pNom: string]: TVirtualBoxClientMachine read GetItemNom write SetItemNom;
    property ItemUUID[pUUID: string]: TVirtualBoxClientMachine read GetItemUUID write SetItemUUID;

    procedure Clear; override;
  end;


  TVirtualBoxClientThread = class(TThread)
  private

  protected
    procedure Execute;

  public
    VirtualBoxClient : TVirtualBoxClient;
    constructor Create(pVirtualBoxClient : TVirtualBoxClient);
    destructor Destroy; override;

  published

  end;


  TVirtualBoxClient = class(TIdTCPClient)
  published

  private
    VirtualBoxClient : TVirtualBoxClient;
    FJournal: TStrings;
    FDossierTravail: string;
    FOnEcrireMessage: TVBMessageEvent;
    FUtilisateur: string;
    FMotDePasse: string;
    FJounaliser: boolean;
    procedure SetJournal(const Value: TStrings);
    procedure SetDossierTravail(const Value: string);
    procedure SetOnEcrireMessage(const Value: TVBMessageEvent);
    procedure SetMotDePasse(const Value: string);
    procedure SetUtilisateur(const Value: string);
    procedure SetJounaliser(const Value: boolean);

  private
    FVBoxManage: string;
    FVersion: string;
    procedure SetVBoxManage(const Value: string);
    procedure SetVersion(const Value: string);
    function CommandeConnexion(pUtilisateur:string;pMotDePasse : string):Boolean;
    function CommandeListMachine : boolean;
    function CommandeMachineActive: boolean;
    function CommandeMachineInfo(Machine : string):string;
    function CommandeControleMachine(Machine : string;Control : TVBMachineControl): boolean;
    function CommandeStartMachine(Machine : string): boolean;
    function CommandeImprimeEcran(Machine : string;FichierPNG : string): boolean;
    function CommandeMachineVRDE(Machine : string): Boolean;
    procedure EcrireMessage(pMessage: string);

  private


  protected
    procedure DoOnConnected; override;
    procedure DoOnDisconnected; override;

  public

    Machines : TVirtualBoxClientMachineList;
    property DossierTravail: string read FDossierTravail write SetDossierTravail;
    property Journal : TStrings read FJournal write SetJournal;
    property Version : string read FVersion write SetVersion;
    property OnEcrireMessage : TVBMessageEvent read FOnEcrireMessage write SetOnEcrireMessage;
    property Utilisateur : string read FUtilisateur write SetUtilisateur;
    property MotDePasse : string read FMotDePasse write SetMotDePasse;
    property Jounaliser : boolean read FJounaliser write SetJounaliser;
    procedure Actualiser;
    procedure MachinesActives;
    function Connexion : boolean;
    procedure Deconnexion;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy;


  published

  end;


implementation

uses SysUtils, IdTCPConnection;

{ TVirtualBoxClient }

procedure TVirtualBoxClient.Actualiser;
begin
  CommandeListMachine;
end;

function TVirtualBoxClient.CommandeListMachine : boolean;
var
  Nom : string;
  UUID : string;
  i,j : integer;
  Nombre : Integer;
  Ligne : string;
  Liste : TStrings;
  Trouver : boolean;
  Suppresion : boolean;
begin
  Result := false;
  try
    EcrireMessage('demande de la liste de machines');
    WriteInteger(VBCodeFonction(VBCFListMachines));
    if ReadInteger = VBCodeFonction(VBCFListMachines) then
    begin
      result := ReadInteger =  VBRequeteResult(VBReqReussi);
      if Result then
      begin
        Nombre := ReadInteger;
        EcrireMessage(format('Re�u %d machines',[Nombre]));
        Liste := TStringList.Create;
        for i := 0 to Nombre - 1 do
        begin
          Ligne := ReadLn;
          TexteVersMachineNomUUID(pChar(Ligne),Nom,UUID);
          EcrireMessage(format('Re�u machine %d/%d "%s"',[i+1,Nombre,Nom]));
          Liste.Add(Ligne);
        end;

        for i := 0 to Liste.Count -1 do
        begin
          Trouver := false;
          TexteVersMachineNomUUID(pChar(Liste[i]),Nom,UUID);
          for j := 0 to Machines.Count - 1 do
          begin
              trouver :=  SameText(Nom,Machines[j].Nom);
              if Trouver then
                break;
          end;
          if not Trouver then
            Machines.Add(Nom,UUID);
        end;

        repeat
          Suppresion := false;
          for i := 0 to Machines.Count -1 do
          begin
            Trouver := false;
            for j := 0 to Liste.Count - 1 do
            begin
              TexteVersMachineNomUUID(pChar(Liste[j]),Nom,UUID);
              trouver :=  SameText(Nom,Machines[i].Nom);
              if Trouver then
                break;
            end;
            if not Trouver then
            begin
              Machines.Remove(Machines[i]);
              Suppresion := true;
              break;
            end;

          end;
        Until not Suppresion;
      end;
    end;
  except
    EcrireMessage('demande impossible');
  end;
end;

function TVirtualBoxClient.CommandeMachineInfo(Machine: string):String;
var
  Taille : integer;
  Info : String;
begin
  result := '';
  try
    EcrireMessage('demande d''information de la machine '+Machine);
    WriteInteger(VBCodeFonction(VBCFMachineInfo));
    WriteLn(Machine);
    if ReadInteger = VBCodeFonction(VBCFMachineInfo) then
    begin
      if ReadInteger =  VBRequeteResult(VBReqReussi) then
      begin
          Taille := ReadInteger;
          Info := ReadString(Taille);
          result := Info;
      end;
    end;
  except
    EcrireMessage('demande impossible');
  end;
end;

constructor TVirtualBoxClient.Create(AOwner: TComponent);
begin
    inherited Create(AOwner);
    Machines := TVirtualBoxClientMachineList.Create(Self);
    FJournal := TStringList.Create;
    Port := VBRS_PORT;
  //Clients := TACSClientList.Create(self);
  FJounaliser := true;






end;

destructor TVirtualBoxClient.Destroy;
begin

end;



procedure TVirtualBoxClient.SetVBoxManage(const Value: string);
begin
  FVBoxManage := Value;
end;

procedure TVirtualBoxClient.SetVersion(const Value: string);
begin
  FVersion := Value;
end;

function TVirtualBoxClient.CommandeControleMachine(Machine: string;
  Control: TVBMachineControl) : boolean;
begin
  result := false;
  try
    EcrireMessage('demande ' +Guillemet(VBMachineControlVersTexte(Control))
        +' de la machine'+Guillemet(Machine));
    WriteInteger(VBCodeFonction(VBCFMachineControl));
    WriteLn(Machine);
    WriteInteger(VBMachineControl(Control));
    if ReadInteger = VBCodeFonction(VBCFMachineControl) then
    begin
      result := ReadInteger =  VBRequeteResult(VBReqReussi);
    end;
  except
    EcrireMessage('demande impossible');
  end;

end;

function TVirtualBoxClient.CommandeStartMachine(Machine: string) : boolean;
begin
  result := false;
  try
    EcrireMessage('demande demarrage de la machine '+Machine);
    WriteInteger(VBCodeFonction(VBCFStartMachine));
    WriteLn(Machine);
    if ReadInteger = VBCodeFonction(VBCFStartMachine) then
    begin
      result := ReadInteger =  VBRequeteResult(VBReqReussi);
    end;
  except
    EcrireMessage('demande impossible');
  end;

end;

function TVirtualBoxClient.CommandeImprimeEcran(Machine: string;FichierPNG : string):boolean;
var
  FichierStream : TFileStream;
  Taille : Integer;
begin

  result := false;
  try
    EcrireMessage('demande imprime ecran machine '+Guillemet(Machine));
    WriteInteger(VBCodeFonction(VBCFScreenshot));
    WriteLn(Machine);
    if ReadInteger = VBCodeFonction(VBCFScreenshot) then
    begin
      result := ReadInteger =  VBRequeteResult(VBReqReussi);
      if Result  then
      begin
        Taille := ReadInteger;
        FichierStream := TFileStream.Create(FichierPNG,fmCreate);
        ReadStream(FichierStream,Taille);
        FichierStream.free;
      end;
    end;
  except
    EcrireMessage('demande impossible');
  end;

  (*
  ShellCommande.LigneCommande := Guillemet(VBoxManage)+' controlvm '  +Guillemet(Machine)
      +' screenshotpng ' +Guillemet(FichierPNG);
  ShellCommande.Execute;
  *)


end;





function TVirtualBoxClient.CommandeMachineActive : boolean;
var
  Nom : String;
  UUID :String;
  i,Nombre : integer;

begin
  Result := false;
  try
    EcrireMessage('Chercher les machines actives');
    WriteInteger(VBCodeFonction(VBCFMachinesActive));
    if ReadInteger = VBCodeFonction(VBCFMachinesActive) then
    begin
      result := ReadInteger =  VBRequeteResult(VBReqReussi);
      if Result then
      begin
        for i := 0 to Machines.Count -1 do
          Machines[i].FActive := false;
          
        Nombre := ReadInteger;
        EcrireMessage(format('Recu %d machines active ',[Nombre]));
        for i := 0 to Nombre - 1 do
        begin
          TexteVersMachineNomUUID(pChar(ReadLn),Nom,UUID);
          EcrireMessage(format('Machine active %d/%d "%s"',[i+1,Nombre,Nom]));
          Machines.ItemNom[Nom].FActive := true;
        end;
      end;

    end;
  except
    EcrireMessage('demande impossible');
  end;
end;

procedure TVirtualBoxClient.MachinesActives;
begin
  CommandeMachineActive;
end;


function TVirtualBoxClient.Connexion:boolean;
begin
  try
    Connect(5000);
    result := Connected;
    
  except
    EcrireMessage('Erreur de connection, verifi� que le serveur � l''adresse "'+Host +'" soit activ�.');
  end;
end;

procedure TVirtualBoxClient.SetJournal(const Value: TStrings);
begin
  FJournal := Value;
end;

function TVirtualBoxClient.CommandeConnexion(pUtilisateur,
  pMotDePasse: string):Boolean;
begin
  try
    EcrireMessage('1/2 Demande de connection de l''utilisateur "'+pUtilisateur+'"');
    WriteInteger(Integer(VBCFConnexion));
    WriteLn(pUtilisateur);
    WriteLn(pMotDePasse);
    if ReadInteger = VBCodeFonction(VBCFConnexion) then
    begin
      if ReadInteger =  VBRequeteResult(VBReqReussi) then
          result := true;
    end;
  except
    EcrireMessage('Connexion impossible');
  end;

end;

procedure TVirtualBoxClient.EcrireMessage(pMessage: string);
var
  Ligne : String;
begin
  if not Jounaliser then
    exit;
  Ligne := DateTimeToStr(now)+#9+pMessage;
  Journal.Insert(0,Ligne);
  if assigned(FOnEcrireMessage) then
      FOnEcrireMessage(Self,Ligne);
end;

procedure TVirtualBoxClient.SetDossierTravail(const Value: string);
begin
  FDossierTravail := Value;
end;

procedure TVirtualBoxClient.Deconnexion;
begin
  Disconnect;
end;

procedure TVirtualBoxClient.SetOnEcrireMessage(
  const Value: TVBMessageEvent);
begin
  FOnEcrireMessage := Value;
end;

procedure TVirtualBoxClient.SetMotDePasse(const Value: string);
begin
  FMotDePasse := Value;
end;

procedure TVirtualBoxClient.SetUtilisateur(const Value: string);
begin
  FUtilisateur := Value;
end;

procedure TVirtualBoxClient.SetJounaliser(const Value: boolean);
begin
  FJounaliser := Value;
end;

procedure TVirtualBoxClient.DoOnConnected;
begin
  
  if CommandeConnexion(FUtilisateur,FMotDePasse) then
  begin
    EcrireMessage('Connection accept�');
//    result := true;
  end
  else
  begin
    EcrireMessage('Connection refus� mot de passe ou utilisateur erron�');
    Disconnect;
  end;
  inherited;

end;

function TVirtualBoxClient.CommandeMachineVRDE(Machine: string): boolean;
var
  Port : integer;
begin
  result := false;
  try
    EcrireMessage('demande lancement du bureau de la machine '+Machine);
    WriteInteger(VBCodeFonction(VBCFMachineVRDE));
    WriteLn(Machine);
    if ReadInteger = VBCodeFonction(VBCFMachineVRDE) then
    begin
      result := ReadInteger =  VBRequeteResult(VBReqReussi);
      if Result then
      begin
        Machines.ItemNom[Machine].VRDEPort := ReadInteger;
      end;
    end;
  except
    EcrireMessage('demande impossible');
  end;

end;

procedure TVirtualBoxClient.DoOnDisconnected;
begin
  inherited;

end;

{ TVirtualBoxClientMachine }

function TVirtualBoxClientMachine.ActiverBureau: boolean;
begin
  result := VirtualBoxClient.CommandeMachineVRDE(Nom);
end;

constructor TVirtualBoxClientMachine.Create(pVirtualBoxClient:TVirtualBoxClient;pNom : string;pUUID : string);
begin
  FNom := pNom;
  FUUID := pUUID;
  VirtualBoxClient := pVirtualBoxClient;
  FVRDEPort := 0;
  FVRDEActif := true;
end;

destructor TVirtualBoxClientMachine.Destroy;
begin

  inherited;

end;

procedure TVirtualBoxClientMachine.Eteindre;
begin
  VirtualBoxClient.CommandeControleMachine(Nom,VBCtrlPowerOff);
end;

procedure TVirtualBoxClientMachine.Extinction;
begin
  VirtualBoxClient.CommandeControleMachine(Nom,VBCtrlAcpiPowerButton);
end;

procedure TVirtualBoxClientMachine.ImprimeEcran(FichierPNG: string);
begin
  VirtualBoxClient.CommandeImprimeEcran(Nom,FichierPNG);
end;

procedure TVirtualBoxClientMachine.Lancer;
begin
  VirtualBoxClient.CommandeStartMachine(Nom);
//  VirtualBoxClient.CommandeControleMachine(nom,VBCtrlResume);
end;

function TVirtualBoxClientMachine.LireInfo : string;
begin
  FInfo :=   VirtualBoxClient.CommandeMachineInfo(Nom);
  result := FInfo;
end;

procedure TVirtualBoxClientMachine.MiseEnVeille;
begin
  VirtualBoxClient.CommandeControleMachine(Nom,VBCtrlAcpiSleepButton);
end;

procedure TVirtualBoxClientMachine.Pause;
begin
  VirtualBoxClient.CommandeControleMachine(Nom,VBCtrlPause);

end;

procedure TVirtualBoxClientMachine.PrendreConrole;
const
  Programme = '"%SystemRoot%\system32\mstsc.exe"';
var
  Commande : string;

begin

  if VRDEPort > 0 then
  begin
    Commande := '/C '+ Programme+' /v '+VirtualBoxClient.Host+':'+IntToStr(VRDEPort);
//    Commande := '/K dir *.*';
    ShellExecute(
            0,
            pChar('open'),
            pChar('cmd.exe'),
            pChar(Commande),
            pChar('C:\'),
            SW_HIDE);
  end;
end;

procedure TVirtualBoxClientMachine.Reprendre;
begin
  VirtualBoxClient.CommandeControleMachine(Nom,VBCtrlResume);

end;

procedure TVirtualBoxClientMachine.Reset;
begin
  VirtualBoxClient.CommandeControleMachine(Nom,VBCtrlReset);
end;

procedure TVirtualBoxClientMachine.Sauver;
begin
  VirtualBoxClient.CommandeControleMachine(Nom,VBCtrlSaveState);
end;

procedure TVirtualBoxClientMachine.SetEtat(const Value: TVBMachineEtat);
begin
  FEtat := Value;
end;

procedure TVirtualBoxClientMachine.SetInfo(const Value: string);
begin
  FInfo := Value;
end;

procedure TVirtualBoxClientMachine.SetNom(const Value: string);
begin
  FNom := Value;
end;

procedure TVirtualBoxClientMachine.SetUUID(const Value: string);
begin
  FUUID := Value;
end;

procedure TVirtualBoxClientMachine.SetVRDEActif(const Value: boolean);
begin
  FVRDEActif := Value;
end;

procedure TVirtualBoxClientMachine.SetVRDEPort(const Value: Integer);
begin
  FVRDEPort := Value;
end;

{ TVirtualBoxClientMachineList }

function TVirtualBoxClientMachineList.Add(Item: TVirtualBoxClientMachine): Integer;
begin
  result := inherited Add(Item);
end;

function TVirtualBoxClientMachineList.Add(pNom, pUUID: string): TVirtualBoxClientMachine;
begin
  Result := TVirtualBoxClientMachine.Create(VirtualBoxClient,pNom,pUUID);
  Add(Result);

end;

procedure TVirtualBoxClientMachineList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;    
  inherited;

end;

constructor TVirtualBoxClientMachineList.Create(pVirtualBoxClient:TVirtualBoxClient);
begin
  VirtualBoxClient := pVirtualBoxClient;

end;

procedure TVirtualBoxClientMachineList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TVirtualBoxClientMachineList.Destroy;
begin
  Clear;
  inherited;
end;

function TVirtualBoxClientMachineList.Extract(Item: TVirtualBoxClientMachine): TVirtualBoxClientMachine;
begin
  result := inherited Extract(Item);
end;

function TVirtualBoxClientMachineList.First: TVirtualBoxClientMachine;
begin
  Result := inherited First;
end;

function TVirtualBoxClientMachineList.Get(Index: Integer): TVirtualBoxClientMachine;
begin
  Result := inherited Get(Index);
end;

function TVirtualBoxClientMachineList.GetItemNom(
  pNom: string): TVirtualBoxClientMachine;
var
 i : integer;
begin
  result := nil;
  for i := 0 to Count - 1 do
    if SameText(Items[i].FNom,pNom) then
    begin
      Result := Items[i];
      exit;
    end;
end;

function TVirtualBoxClientMachineList.GetItemUUID(
  pUUID: string): TVirtualBoxClientMachine;
var
 i : integer;
begin
  result := nil;
  for i := 0 to Count - 1 do
    if SameText(Items[i].FUUID,pUUID) then
    begin
      Result := Items[i];
      exit;
    end;

end;


function TVirtualBoxClientMachineList.IndexOf(Item: TVirtualBoxClientMachine): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TVirtualBoxClientMachineList.Insert(Index: Integer; Item: TVirtualBoxClientMachine);
begin
  inherited Insert(Index,Item);
end;

function TVirtualBoxClientMachineList.Last: TVirtualBoxClientMachine;
begin
  Result := inherited Last;
end;

procedure TVirtualBoxClientMachineList.Put(Index: Integer; Item: TVirtualBoxClientMachine);
begin
  inherited Put(Index,Item);

end;

function TVirtualBoxClientMachineList.Remove(Item: TVirtualBoxClientMachine): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;


procedure TVirtualBoxClientMachineList.SetItemNom(pNom: string;
  const Value: TVirtualBoxClientMachine);
var
 i : integer;
begin

  for i := 0 to Count - 1 do
    if SameText(Items[i].FNom,pNom) then
    begin
      Items[i] := Value;
      exit;
    end;

end;

procedure TVirtualBoxClientMachineList.SetItemUUID(pUUID: string;
  const Value: TVirtualBoxClientMachine);
var
 i : integer;
begin

  for i := 0 to Count - 1 do
    if SameText(Items[i].FUUID,pUUID) then
    begin
      Items[i] := Value;
      exit;
    end;

end;

{ TVirtualBoxClientThread }

constructor TVirtualBoxClientThread.Create(
  pVirtualBoxClient: TVirtualBoxClient);
begin
  inherited Create(false);

end;

destructor TVirtualBoxClientThread.Destroy;
begin

  inherited;
end;

procedure TVirtualBoxClientThread.Execute;
begin
  while not Terminated do
  begin

  end;
end;

end.
