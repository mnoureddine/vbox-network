unit VBRSParametres;

interface

//{$DEFINE DEBUG} 
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,VBRSDonnees,FileCtrl, Spin,VBUtiles;

type
  TFormVBParametres = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ButtonAnnuler: TButton;
    ButtonOK: TButton;
    Label1: TLabel;
    EditVBoxManage: TEdit;
    ButtonVboxManageParcourir: TButton;
    Label2: TLabel;
    EditDossierTravail: TEdit;
    ButtonDossierTravailParcourir: TButton;
    Label3: TLabel;
    SpinEditPort: TSpinEdit;
    GroupBox1: TGroupBox;
    CheckBoxModeHeadless: TCheckBox;
    CheckBoxServeurActifAuDemarrage: TCheckBox;
    EditServeurUtilisateur: TEdit;
    EditServeurMotDePasse: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    CheckBoxServeurJournaliser: TCheckBox;
    TabSheet2: TTabSheet;
    CheckBoxShellCommandeActiver: TCheckBox;
    CheckBoxShellCommandeObservateur: TCheckBox;
    CheckBoxBarreDesTachesDemarrage: TCheckBox;
    Label6: TLabel;
    ComboBoxLangue: TComboBox;
    TabSheet3: TTabSheet;
    CheckBoxWebActif: TCheckBox;
    SpinEditWebPort: TSpinEdit;
    Label7: TLabel;
    Label8: TLabel;
    SpinEditWebRafraich: TSpinEdit;
    Label9: TLabel;
    procedure ButtonDossierTravailParcourirClick(Sender: TObject);
    procedure ButtonVboxManageParcourirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

    function GetVBoxManage: string;
    procedure SetVBoxManage(const Value: string);
    function GetDossierTravail: string;
    procedure SetDossierTravail(const Value: string);
    function GetModeHeadless: boolean;
    function GetServeurActifAuDemmarrage: boolean;
    procedure SetModeHeadless(const Value: boolean);
    procedure SetServeurActifAuDemmarrage(const Value: boolean);
    function GetServeurMotDePasse: string;
    function GetServeurUtilisateur: string;
    procedure SetServeurMotDePasse(const Value: string);
    procedure SetServeurUtilisateur(const Value: string);
    function GetServeurPort: integer;
    procedure SetServeurPort(const Value: integer);
    procedure SetServeurJournaliser(const Value: boolean);
    procedure SetShellCommandeActiver(const Value: boolean);
    procedure SetShellCommandeObservateur(const Value: boolean);
    function GetServeurJournaliser: boolean;
    function GetShellCommandeActiver: boolean;
    function GetShellCommandeObservateur: boolean;
    procedure SetBarreDesTachesDemarrage(const Value: boolean);
    function GetBarreDesTachesDemarrage: boolean;
    { Déclarations privées }
  public
    { Déclarations publiques }
    property VBoxManage : string read GetVBoxManage write SetVBoxManage;
    property DossierTravail : string read GetDossierTravail write SetDossierTravail;
    property ModeHeadless : boolean read GetModeHeadless write SetModeHeadless;
    property ServeurActifAuDemmarrage : boolean read GetServeurActifAuDemmarrage write SetServeurActifAuDemmarrage;
    property ServeurPort : integer read GetServeurPort write SetServeurPort;
    property ServeurUtilisateur : string read GetServeurUtilisateur write SetServeurUtilisateur;
    property ServeurMotDePasse : string read GetServeurMotDePasse write SetServeurMotDePasse;
    property ServeurJournaliser: boolean  read GetServeurJournaliser write SetServeurJournaliser;
    property  ShellCommandeActiver : boolean read GetShellCommandeActiver write SetShellCommandeActiver;
    property  ShellCommandeObservateur : boolean read GetShellCommandeObservateur write SetShellCommandeObservateur;
    property BarreDesTachesDemarrage : boolean read GetBarreDesTachesDemarrage write SetBarreDesTachesDemarrage;
    procedure ChargerParametres;
    procedure EnregistrerParametres;
    function executer : boolean;
  end;


implementation

{$R *.dfm}

{ TFormVBParametres }

procedure TFormVBParametres.ChargerParametres;
begin
  VBoxManage := Donnees.Parametres.VirtualBox.VBoxManage;
  DossierTravail := Donnees.Parametres.VirtualBox.DossierTravail;
  ModeHeadless := Donnees.Parametres.VirtualBox.ModeHeadless;
  BarreDesTachesDemarrage := Donnees.Parametres.General.BarreDesTachesDemarrage;
  ServeurActifAuDemmarrage := Donnees.Parametres.Serveur.ActifDemarrage;
  ServeurPort := Donnees.Parametres.Serveur.Port;
  ServeurUtilisateur := Donnees.Parametres.Serveur.Utilisateur;
  ServeurMotDePasse := Donnees.Parametres.Serveur.MotDePasse;
  ServeurJournaliser := Donnees.Parametres.Serveur.Journaliser;
  ShellCommandeActiver := Donnees.Parametres.ShellCommand.Activer;
  ShellCommandeObservateur := Donnees.Parametres.ShellCommand.Observateur;
  CheckBoxWebActif.Checked :=  Donnees.Parametres.Serveur.WebActif;
  SpinEditWebPort.Value := Donnees.Parametres.Serveur.WebPort;
  SpinEditWebRafraich.Value := Donnees.Parametres.Serveur.WebRafraich;



end;

procedure TFormVBParametres.EnregistrerParametres;
begin
  Donnees.Parametres.General.Language := ComboBoxLangue.Text;
  Donnees.Parametres.VirtualBox.VBoxManage := VBoxManage;
  Donnees.Parametres.VirtualBox.DossierTravail := DossierTravail;
  Donnees.Parametres.VirtualBox.ModeHeadless := ModeHeadless;
  Donnees.Parametres.General.BarreDesTachesDemarrage := BarreDesTachesDemarrage;
  Donnees.Parametres.Serveur.ActifDemarrage := ServeurActifAuDemmarrage;
  Donnees.Parametres.Serveur.Port := ServeurPort;
  Donnees.Parametres.Serveur.Utilisateur := ServeurUtilisateur;
  Donnees.Parametres.Serveur.MotDePasse := ServeurMotDePasse;
  Donnees.Parametres.Serveur.Journaliser := ServeurJournaliser;
  Donnees.Parametres.ShellCommand.Activer := ShellCommandeActiver;
  Donnees.Parametres.ShellCommand.Observateur := ShellCommandeObservateur;
  Donnees.Parametres.Serveur.WebActif := CheckBoxWebActif.Checked;
  Donnees.Parametres.Serveur.WebPort := SpinEditWebPort.Value;
  Donnees.Parametres.Serveur.WebRafraich := SpinEditWebRafraich.Value;
  Donnees.EnregistrerParametres;
end;

function TFormVBParametres.executer: boolean;
begin
  ChargerParametres;
  result := ShowModal = mrOK;
  if Result then
    EnregistrerParametres;
end;

function TFormVBParametres.GetVBoxManage: string;
begin
  Result := EditVBoxManage.Text;
end;

procedure TFormVBParametres.SetVBoxManage(const Value: string);
begin
  EditVBoxManage.Text := Value;
end;

procedure TFormVBParametres.ButtonDossierTravailParcourirClick(
  Sender: TObject);
var
  Dossier:string;
begin
  Dossier := DossierTravail;
  if SelectDirectory('Chemin du dossier de travail','',Dossier) then
  begin
    DossierTravail := Dossier
  end;
end;

function TFormVBParametres.GetDossierTravail: string;
begin
  result := EditDossierTravail.Text;
end;

procedure TFormVBParametres.SetDossierTravail(const Value: string);
begin
  EditDossierTravail.Text := Value;
end;

function TFormVBParametres.GetModeHeadless: boolean;
begin
  result := CheckBoxModeHeadless.Checked;
end;

function TFormVBParametres.GetServeurActifAuDemmarrage: boolean;
begin
  result := CheckBoxServeurActifAuDemarrage.Checked;
end;

procedure TFormVBParametres.SetModeHeadless(const Value: boolean);
begin
  CheckBoxModeHeadless.Checked := Value;
end;

procedure TFormVBParametres.SetServeurActifAuDemmarrage(
  const Value: boolean);
begin
  CheckBoxServeurActifAuDemarrage.Checked := Value;
end;

function TFormVBParametres.GetServeurMotDePasse: string;
begin
  Result := EditServeurMotDePasse.Text;
end;

function TFormVBParametres.GetServeurUtilisateur: string;
begin
  Result := EditServeurUtilisateur.Text;
end;

procedure TFormVBParametres.SetServeurMotDePasse(const Value: string);
begin
  EditServeurMotDePasse.Text := Value;
end;

procedure TFormVBParametres.SetServeurUtilisateur(const Value: string);
begin
   EditServeurUtilisateur.Text := Value;
end;

function TFormVBParametres.GetServeurPort: integer;
begin
  Result := SpinEditPort.Value;
end;

procedure TFormVBParametres.SetServeurPort(const Value: integer);
begin
  SpinEditPort.Value := Value;
end;

procedure TFormVBParametres.ButtonVboxManageParcourirClick(
  Sender: TObject);
begin
  With TOpenDialog.Create(self) do
  begin
    Filter := 'Application VBoxManage|*.exe|Tous les fichier(*.*)|*.*';
    FileName := VBoxManage;
    if Execute then
      VBoxManage := FileName;
    free;
  end;
end;

procedure TFormVBParametres.SetServeurJournaliser(const Value: boolean);
begin
  CheckBoxServeurJournaliser.Checked := Value;
end;

procedure TFormVBParametres.SetShellCommandeActiver(const Value: boolean);
begin
  CheckBoxShellCommandeActiver.Checked := Value;
end;

procedure TFormVBParametres.SetShellCommandeObservateur(
  const Value: boolean);
begin
  CheckBoxShellCommandeObservateur.Checked := Value;
end;

function TFormVBParametres.GetServeurJournaliser: boolean;
begin
  Result := CheckBoxServeurJournaliser.Checked;
end;

function TFormVBParametres.GetShellCommandeActiver: boolean;
begin
    Result := CheckBoxShellCommandeActiver.Checked;
end;

function TFormVBParametres.GetShellCommandeObservateur: boolean;
begin
    Result := CheckBoxShellCommandeObservateur.Checked;
end;

procedure TFormVBParametres.SetBarreDesTachesDemarrage(
  const Value: boolean);
begin
  CheckBoxBarreDesTachesDemarrage.Checked := Value;
end;

function TFormVBParametres.GetBarreDesTachesDemarrage: boolean;
begin
      Result := CheckBoxBarreDesTachesDemarrage.Checked;
end;

procedure TFormVBParametres.FormCreate(Sender: TObject);
var
  sr:TSearchRec;
begin
  ComboBoxLangue.Items.Clear;
  if FindFirst(LanguagesDir+'\*.lng',faAnyFile,sr) = 0 then
  begin
    repeat
      ComboBoxLangue.Items.Add(sr.Name)
    until FindNext(sr)<>0;
    FindClose(sr);
  end;
  PageControl1.ActivePageIndex := 0;

  ComboBoxLangue.Text := Donnees.Parametres.General.Language;

   {$IFNDEF DEBUG}
      TranslateAll(Self,LanguageFile);
   {$ENDIF}
end;

end.
