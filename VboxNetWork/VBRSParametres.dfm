object FormVBParametres: TFormVBParametres
  Left = 458
  Top = 256
  BorderStyle = bsDialog
  Caption = 'Parametres'
  ClientHeight = 475
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label8: TLabel
    Left = 104
    Top = 109
    Width = 28
    Height = 13
    Caption = 'Port : '
    FocusControl = SpinEditWebRafraich
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 385
    Height = 425
    ActivePage = TabSheet3
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '&VirtualBox'
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 193
        Height = 13
        Caption = 'Chemin de l'#39'application VirtualBox'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 16
        Top = 72
        Width = 100
        Height = 13
        Caption = 'Dossier de travail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 57
        Top = 172
        Width = 42
        Height = 13
        Caption = 'Langue :'
        FocusControl = ComboBoxLangue
      end
      object EditVBoxManage: TEdit
        Left = 16
        Top = 32
        Width = 281
        Height = 21
        TabOrder = 0
      end
      object ButtonVboxManageParcourir: TButton
        Left = 304
        Top = 32
        Width = 51
        Height = 25
        Caption = '...'
        TabOrder = 1
        OnClick = ButtonVboxManageParcourirClick
      end
      object EditDossierTravail: TEdit
        Left = 16
        Top = 88
        Width = 281
        Height = 21
        TabOrder = 2
      end
      object ButtonDossierTravailParcourir: TButton
        Left = 304
        Top = 88
        Width = 51
        Height = 25
        Caption = '...'
        TabOrder = 3
        OnClick = ButtonDossierTravailParcourirClick
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 200
        Width = 337
        Height = 185
        Caption = 'Serveur TCP'
        TabOrder = 4
        object Label3: TLabel
          Left = 45
          Top = 54
          Width = 28
          Height = 13
          Caption = 'Port : '
          FocusControl = SpinEditPort
        end
        object Label4: TLabel
          Left = 27
          Top = 86
          Width = 52
          Height = 13
          Caption = 'Utilisateur :'
          FocusControl = EditServeurUtilisateur
        end
        object Label5: TLabel
          Left = 11
          Top = 118
          Width = 73
          Height = 13
          Caption = 'Mot de passe : '
          FocusControl = EditServeurMotDePasse
        end
        object SpinEditPort: TSpinEdit
          Left = 96
          Top = 48
          Width = 121
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
        end
        object CheckBoxServeurActifAuDemarrage: TCheckBox
          Left = 16
          Top = 24
          Width = 193
          Height = 17
          Caption = 'Actif au d'#233'marrage'
          TabOrder = 1
        end
        object EditServeurUtilisateur: TEdit
          Left = 96
          Top = 80
          Width = 177
          Height = 21
          TabOrder = 2
        end
        object EditServeurMotDePasse: TEdit
          Left = 96
          Top = 116
          Width = 177
          Height = 21
          PasswordChar = '*'
          TabOrder = 3
        end
        object CheckBoxServeurJournaliser: TCheckBox
          Left = 16
          Top = 152
          Width = 153
          Height = 17
          Caption = 'Journal de communication'
          TabOrder = 4
        end
      end
      object CheckBoxModeHeadless: TCheckBox
        Left = 16
        Top = 120
        Width = 329
        Height = 17
        Caption = 'Mode headless : sans affichage des machine virtuelles'
        TabOrder = 5
      end
      object CheckBoxBarreDesTachesDemarrage: TCheckBox
        Left = 16
        Top = 144
        Width = 329
        Height = 17
        Caption = 'Mettre dans la barre des taches au d'#233'marrage.'
        TabOrder = 6
      end
      object ComboBoxLangue: TComboBox
        Left = 112
        Top = 168
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 7
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Ligne de commande'
      ImageIndex = 1
      object CheckBoxShellCommandeActiver: TCheckBox
        Left = 16
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Activer'
        TabOrder = 0
      end
      object CheckBoxShellCommandeObservateur: TCheckBox
        Left = 16
        Top = 56
        Width = 345
        Height = 17
        Caption = 'Observer les commande provenat du serveur'
        TabOrder = 1
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Web Serveur'
      ImageIndex = 2
      object Label7: TLabel
        Left = 120
        Top = 45
        Width = 28
        Height = 13
        Caption = 'Port : '
        FocusControl = SpinEditWebPort
      end
      object Label9: TLabel
        Left = 24
        Top = 77
        Width = 124
        Height = 13
        Caption = 'Auto rafraichissement (s) : '
        FocusControl = SpinEditWebRafraich
      end
      object CheckBoxWebActif: TCheckBox
        Left = 16
        Top = 16
        Width = 209
        Height = 17
        Caption = 'Activer le serveur web'
        TabOrder = 0
      end
      object SpinEditWebPort: TSpinEdit
        Left = 156
        Top = 40
        Width = 81
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 8080
      end
      object SpinEditWebRafraich: TSpinEdit
        Left = 156
        Top = 72
        Width = 81
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 8
      end
    end
  end
  object ButtonAnnuler: TButton
    Left = 320
    Top = 440
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Annuler'
    ModalResult = 2
    TabOrder = 1
  end
  object ButtonOK: TButton
    Left = 240
    Top = 440
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
end
