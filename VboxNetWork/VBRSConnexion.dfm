object FormVBRCConnexion: TFormVBRCConnexion
  Left = 1748
  Top = 281
  BorderStyle = bsDialog
  Caption = 'boite de connexion'
  ClientHeight = 220
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 43
    Height = 13
    Caption = 'Serveur :'
  end
  object Label2: TLabel
    Left = 40
    Top = 56
    Width = 25
    Height = 13
    Caption = 'Port :'
    FocusControl = SpinEditPort
  end
  object Label3: TLabel
    Left = 40
    Top = 88
    Width = 55
    Height = 13
    Caption = 'Utilisateur : '
    FocusControl = EditUtilisateur
  end
  object Label4: TLabel
    Left = 32
    Top = 120
    Width = 73
    Height = 13
    Caption = 'Mot de passe : '
    FocusControl = EditMotDePasse
  end
  object EditServeur: TEdit
    Left = 136
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'localhost'
  end
  object EditUtilisateur: TEdit
    Left = 136
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object EditMotDePasse: TEdit
    Left = 136
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object ButtonAnnuler: TButton
    Left = 168
    Top = 184
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Annuler'
    ModalResult = 2
    TabOrder = 3
  end
  object ButtonConnxion: TButton
    Left = 88
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Connexion'
    Default = True
    ModalResult = 1
    TabOrder = 4
  end
  object SpinEditPort: TSpinEdit
    Left = 136
    Top = 48
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 4512
  end
  object CheckBoxEnregistrerMotDePasse: TCheckBox
    Left = 64
    Top = 144
    Width = 209
    Height = 17
    Caption = 'enrregistrer le mot de passe.'
    TabOrder = 6
  end
end
