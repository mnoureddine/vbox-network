unit VBRSConnexion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls,VBRCDonnees;

type
  TFormVBRCConnexion = class(TForm)
    Label1: TLabel;
    EditServeur: TEdit;
    EditUtilisateur: TEdit;
    EditMotDePasse: TEdit;
    ButtonAnnuler: TButton;
    ButtonConnxion: TButton;
    SpinEditPort: TSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CheckBoxEnregistrerMotDePasse: TCheckBox;
  private
    procedure SetEnregistrerMotDePasse(const Value: boolean);
    procedure SetMotDePasse(const Value: string);
    procedure SetPort(const Value: Integer);
    procedure SetServeur(const Value: string);
    procedure SetUtilisateur(const Value: string);
    function GetEnregistrerMotDePasse: boolean;
    function GetMotDePasse: string;
    function GetPort: Integer;
    function GetServeur: string;
    function GetUtilisateur: string;
    { Déclarations privées }
  public
    { Déclarations publiques }
    property Serveur : string read GetServeur write SetServeur;
    property Port : Integer read GetPort write SetPort;
    property Utilisateur : string read GetUtilisateur write SetUtilisateur;
    property MotDePasse : string read GetMotDePasse write SetMotDePasse;
    property EnregistrerMotDePasse: boolean read GetEnregistrerMotDePasse write SetEnregistrerMotDePasse;
    procedure Charger;
    procedure Enregsitrer;
    function Executer : boolean;
  end;

var
  FormVBRCConnexion: TFormVBRCConnexion;

implementation

{$R *.dfm}

{ TForm1 }

procedure TFormVBRCConnexion.Charger;
begin
  Donnees.VirtualBox
end;

procedure TFormVBRCConnexion.Enregsitrer;
begin

end;

function TFormVBRCConnexion.Executer: boolean;
begin

end;

function TFormVBRCConnexion.GetEnregistrerMotDePasse: boolean;
begin
  Result := CheckBoxEnregistrerMotDePasse.Checked;
end;

function TFormVBRCConnexion.GetMotDePasse: string;
begin
  Result := EditMotDePasse.Text;
end;

function TFormVBRCConnexion.GetPort: Integer;
begin
  Result := SpinEditPort.Value;
end;

function TFormVBRCConnexion.GetServeur: string;
begin
  Result := EditServeur.Text;
end;

function TFormVBRCConnexion.GetUtilisateur: string;
begin

  Result := EditUtilisateur.Text;
end;

procedure TFormVBRCConnexion.SetEnregistrerMotDePasse(const Value: boolean);
begin
  CheckBoxEnregistrerMotDePasse.Checked := Value;
end;

procedure TFormVBRCConnexion.SetMotDePasse(const Value: string);
begin
  EditMotDePasse.Text:= Value;
end;

procedure TFormVBRCConnexion.SetPort(const Value: Integer);
begin
  SpinEditPort.Value := Value;
end;

procedure TFormVBRCConnexion.SetServeur(const Value: string);
begin
  EditServeur.Text := Value;
end;

procedure TFormVBRCConnexion.SetUtilisateur(const Value: string);
begin
  EditUtilisateur.Text := Value;
end;

end.
