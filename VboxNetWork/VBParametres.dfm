object FormVBParametres: TFormVBParametres
  Left = 458
  Top = 256
  Width = 412
  Height = 508
  Caption = 'Parametres'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 385
    Height = 425
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '&VirtualBox'
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 70
        Height = 13
        Caption = 'Informations'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object StaticText1: TStaticText
        Left = 16
        Top = 32
        Width = 337
        Height = 41
        AutoSize = False
        BorderStyle = sbsSingle
        TabOrder = 0
      end
      object EditVBoxManage: TEdit
        Left = 16
        Top = 88
        Width = 281
        Height = 21
        TabOrder = 1
      end
      object ButtonVboxManageParcourir: TButton
        Left = 304
        Top = 88
        Width = 51
        Height = 25
        Caption = '...'
        TabOrder = 2
      end
    end
  end
  object Button1: TButton
    Left = 320
    Top = 440
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Annuler'
    TabOrder = 1
  end
  object Button2: TButton
    Left = 240
    Top = 440
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 2
  end
end
