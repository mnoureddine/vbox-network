unit VBUtiles;

{$DEFINE DEBUG}
interface
uses
  Windows,SysUtils,Classes,TypInfo;
function GetTempDir: string;
procedure TranslateAllComponents(Component : TComponent;Translate:TStrings);
procedure TranslateAll(Component : TComponent;FileName:String);

implementation

uses Math;

function GetTempDir: string;
var
  Buffer: array[0..MAX_PATH] of Char;
begin
  GetTempPath(SizeOf(Buffer) - 1, Buffer);
  Result := StrPas(Buffer);
end;

function GetComponentCaption(Component : TComponent):String;
begin
  try
    result := GetPropValue(Component,'Caption',true)
  except
    result := ''
  end;
end;

procedure TranslateAllComponents(Component : TComponent;Translate:TStrings);
  function getTranslate(Text : string):string;
  var
    i : integer;
  begin
    if Text = '' then begin result := '';  exit; end;
    for i := 0 to Translate.Count - 1 do
      if SameText(Translate.Names[i],Text) then
      begin
        result := Translate.ValueFromIndex[i];
        if Result ='' then
            result := Text;
        exit;
      end;
    Result := Text;
    Translate.Add(Text+'=')
  end;

  procedure TranslateComponent(Comp : TComponent);
  begin
    try

      SetPropValue(Comp,'Caption',getTranslate(GetPropValue(Comp,'Caption',true)));
    except

    end;
  end;

var
    j : integer;

begin
     {$IFNDEF DEBUG}
  TranslateComponent(Component);
  for j := 0 to Component.ComponentCount -1 do
    TranslateAllComponents(Component.Components[j],Translate);
      {$ENDIF}
end;

procedure TranslateAll(Component : TComponent;FileName:String);
var
  Translate : TStrings;
  TranslateLineCount : integer;
begin
  Translate := TStringList.Create;
  if FileExists(FileName)then
    Translate.LoadFromFile(FileName);
  TranslateLineCount := Translate.Count;
  TranslateAllComponents(Component,Translate);
  if TranslateLineCount < Translate.Count then
    Translate.SaveToFile(FileName);
end;

end.
