unit ShellCommande;

interface
uses
  Classes,SysUtils,ShellApi,Windows;

type


  TShellComandeEvent = procedure(Sender: TObject;Resultat:String)of object;
  TShellCommande = class(TObject)
  private
    FDossierTravail: string;
    FResultat: TStrings;
    FCommande: string;
    FLigneCommande: string;
    FOnExecute: TShellComandeEvent;
    procedure SetDossierTravail(const Value: string);
    procedure SetResultat(const Value: TStrings);
    procedure SetLigneCommande(const Value: string);
    procedure SetOnExecute(const Value: TShellComandeEvent);
  protected

  public
    property OnExecute : TShellComandeEvent read FOnExecute write SetOnExecute;
    constructor  Create;overload;
    constructor  Create(pDossierTravail : string);overload;
    constructor  Create(pDossierTravail : string;pLigneCommande :string);overload;
    property DossierTravail : string read FDossierTravail write SetDossierTravail;
    property Resultat : TStrings read FResultat write SetResultat;
    property LigneCommande : string read FLigneCommande write SetLigneCommande;
    function Execute : boolean;overload;
    function Execute(pLigneCommande :string):boolean;overload;
    function Execute(pDossierTravail : string;pLigneCommande :string):boolean;overload;
    procedure ExecuteEx(pDossierTravail : string;pLigneCommande :string);overload;



  published

  end;
implementation

function GetDosOutput(CommandLine: string; Work: string = 'C:\'): string;
var
  SA: TSecurityAttributes;
  SI: TStartupInfo;
  PI: TProcessInformation;
  StdOutPipeRead, StdOutPipeWrite: THandle;
  WasOK: Boolean;
  Buffer: array[0..255] of AnsiChar;
  BytesRead: Cardinal;
  WorkDir: string;
  Handle: Boolean;
begin
  Result := '';
  with SA do begin
    nLength := SizeOf(SA);
    bInheritHandle := True;
    lpSecurityDescriptor := nil;
  end;
  CreatePipe(StdOutPipeRead, StdOutPipeWrite, @SA, 0);
  try
    with SI do
    begin
      FillChar(SI, SizeOf(SI), 0);
      cb := SizeOf(SI);
      dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      wShowWindow := SW_HIDE;
      hStdInput := GetStdHandle(STD_INPUT_HANDLE); // don't redirect stdin
      hStdOutput := StdOutPipeWrite;
      hStdError := StdOutPipeWrite;
    end;
    WorkDir := Work;
    Handle := CreateProcess(nil, PChar('cmd.exe /C ' + CommandLine),
                            nil, nil, True, 0, nil,
                            PChar(WorkDir), SI, PI);
    CloseHandle(StdOutPipeWrite);
    if Handle then
      try
        repeat
          WasOK := ReadFile(StdOutPipeRead, Buffer, 255, BytesRead, nil);
          if BytesRead > 0 then
          begin
            Buffer[BytesRead] := #0;
            Result := Result + Buffer;
          end;
        until not WasOK or (BytesRead = 0);
        WaitForSingleObject(PI.hProcess, INFINITE);
      finally
        CloseHandle(PI.hThread);
        CloseHandle(PI.hProcess);
      end;
  finally
    CloseHandle(StdOutPipeRead);
  end;
end;

{ TShellCommande }

constructor TShellCommande.Create(pDossierTravail: string);
begin
    Create;
    FDossierTravail := pDossierTravail;
end;

function TShellCommande.Execute:boolean;
begin
  result := Execute(FDossierTravail,FLigneCommande);

//  ExecuteEx(FDossierTravail,FLigneCommande);
end;

function TShellCommande.Execute(pLigneCommande :string):boolean;
begin
  result := Execute(FDossierTravail,pLigneCommande);
end;

constructor TShellCommande.Create;
begin
  FResultat := TStringList.Create;
  FCommande := '';
  FDossierTravail := '';
  FLigneCommande := '';
  FOnExecute := nil;
end;



procedure TShellCommande.SetDossierTravail(const Value: string);
begin
  FDossierTravail := Value;
end;

procedure TShellCommande.SetResultat(const Value: TStrings);
begin
  FResultat := Value;
end;

function TShellCommande.Execute( pDossierTravail,
  pLigneCommande: string):boolean;
begin
  result := true;
  try
    FResultat.SetText(pChar(GetDosOutput('"'+FLigneCommande+'"',FDossierTravail)));
    if Assigned(OnExecute) then
      OnExecute(Self,FLigneCommande+SlIneBreak+ FResultat.GetText);
  except
    result := false;
  end;

end;



procedure TShellCommande.SetLigneCommande(const Value: string);
begin
  FLigneCommande := Value;
end;


procedure TShellCommande.ExecuteEx(pDossierTravail, pLigneCommande: string);
var
  ExecInfo: TShellExecuteInfoA;
  FichierResultat : string;
begin
(*
    FichierResultat := GetCurrentDir+'\Resultat.txt';

    ShellExecute(0,                // Handle d'une fen�tre
    'open',                      // Op�ration � effectuer
    'cmd.exe',
    pChar('/C '+pLigneCommande+' >"'+FichierResultat+'"'),
    pChar(pDossierTravail),   // R�pertoire par d�faut
    SW_HIDE);
    sleep(1000);
    FResultat.LoadFromFile(FichierResultat);
end;
  *)
  FichierResultat := GetCurrentDir+'\Resultat.txt';
  ZeroMemory(@ExecInfo, sizeof(ExecInfo));

  with ExecInfo do
  begin
    cbSize := sizeOf(ExecInfo);
    fMask  := SEE_MASK_NOCLOSEPROCESS;
    lpVerb := PChar('open');
    lpFile := PChar('cmd.exe');
    lpParameters := pChar('/C '+pLigneCommande+' >"'+FichierResultat+'"');
    lpDirectory := pChar(pDossierTravail);
    nShow  := SW_HIDE;
  end;
  ShellExecuteEx(@ExecInfo);
  if  ExecInfo.hInstApp<32  then
  begin
    WaitForInputIdle(ExecInfo.hProcess, 10000);
  end;
  FResultat.LoadFromFile(FichierResultat);

end;

constructor TShellCommande.Create(pDossierTravail, pLigneCommande: string);
begin
  Create(pDossierTravail);
  FLigneCommande := pLigneCommande;
end;


procedure TShellCommande.SetOnExecute(const Value: TShellComandeEvent);
begin
  FOnExecute := Value;
end;

end.
