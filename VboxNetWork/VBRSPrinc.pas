unit VBRSPrinc;

interface

//{$DEFINE DEBUG}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ShellApi, StdCtrls, ExtCtrls,FileCtrl,IniFiles, ComCtrls,ShellCommande,VBNVirtualBox,VBUtiles,VBRSDonnees,
  Menus, ActnList, ImgList,PNGImage,VBRSParametres,GetVersion, Grids,
  ValEdit, JvComponentBase, JvTrayIcon, Buttons, ToolWin,VBRSMachinePropriete,
  XPMan,StrUtils;

type

  TFormPrinc = class(TForm)
    LabeledEditDossierTravail: TLabeledEdit;
    MemoResultat: TMemo;
    ButtonPacourir: TButton;
    Label2: TLabel;
    ButtonExecuter: TButton;
    OpenDialog1: TOpenDialog;
    ComboBoxLignesCommande: TComboBox;
    Label3: TLabel;
    PageControl1: TPageControl;
    TabSheetShellCommande: TTabSheet;
    TabSheet2: TTabSheet;
    TreeViewMachines: TTreeView;
    ImageMachine: TImage;
    Panel1: TPanel;
    MainMenu1: TMainMenu;
    Machines1: TMenuItem;
    Actualiser1: TMenuItem;
    ActionList1: TActionList;
    ActionActualiserMachines: TAction;
    ImageListGrand: TImageList;
    Panel2: TPanel;
    TimerActualiser: TTimer;
    Panel3: TPanel;
    ImageVBoxMachine: TImage;
    LabelMachineNom: TLabel;
    LabelMachineUUID: TLabel;
    ListBoxMachineInfo: TListBox;
    Label1: TLabel;
    ActionActiverServeur: TAction;
    ListBoxJournal: TListBox;
    LabelJournal: TLabel;
    Activer1: TMenuItem;
    ActionActualiserImage: TAction;
    ActionParametres: TAction;
    Parametres1: TMenuItem;
    StatusBar1: TStatusBar;
    N1: TMenuItem;
    Quitter1: TMenuItem;
    ActionQuitter: TAction;
    Panel4: TPanel;
    Splitter1: TSplitter;
    JvTrayIcon1: TJvTrayIcon;
    PopupMenuTaches: TPopupMenu;
    Activer2: TMenuItem;
    Parametres2: TMenuItem;
    Quitter2: TMenuItem;
    N2: TMenuItem;
    ActionAfficher: TAction;
    Afficher1: TMenuItem;
    ImageListBarreDesTaches: TImageList;
    PopupMenuCommand: TPopupMenu;
    Lancer1: TMenuItem;
    Extinction1: TMenuItem;
    Pauser1: TMenuItem;
    Reprendre1: TMenuItem;
    Reset1: TMenuItem;
    Eteindre1: TMenuItem;
    Sauver1: TMenuItem;
    ToolBarMachine: TToolBar;
    ToolButtonLancer: TToolButton;
    Extinction: TToolButton;
    Autre: TToolButton;
    MainMenuAutre: TMainMenu;
    ActionListMachine: TActionList;
    ActionLancer: TAction;
    ActionExtinction: TAction;
    ActionEteindre: TAction;
    ActionPause: TAction;
    ActionReprendre: TAction;
    ActionReset: TAction;
    ActionSauver: TAction;
    ActionMiseEnVeille: TAction;
    Autre1: TMenuItem;
    Lancer2: TMenuItem;
    Eteindre2: TMenuItem;
    Extinction2: TMenuItem;
    Pause1: TMenuItem;
    Extinction3: TMenuItem;
    Sauver2: TMenuItem;
    Miseenveille1: TMenuItem;
    Miseenveille2: TMenuItem;
    ToolButton1: TToolButton;
    N3: TMenuItem;
    PopupMachines: TMenuItem;
    ButtonActualiserImage: TButton;
    ActionPrendreLeControle: TAction;
    ImageListMenuMachine: TImageList;
    ActionMachinePropriete: TAction;
    Proprit1: TMenuItem;
    Proprit2: TMenuItem;
    Panel5: TPanel;
    XPManifest1: TXPManifest;
    Splitter2: TSplitter;
    ToolButton2: TToolButton;
    LabelMachineState: TLabel;
    procedure ButtonPacourirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonExecuterClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionActualiserMachinesExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeViewMachinesClick(Sender: TObject);
    procedure TimerActualiserTimer(Sender: TObject);
    procedure ActionActiverServeurExecute(Sender: TObject);
    procedure ActionActualiserImageExecute(Sender: TObject);
    procedure ActionParametresExecute(Sender: TObject);
    procedure ActionQuitterExecute(Sender: TObject);
    procedure ActionAfficherExecute(Sender: TObject);
    procedure ActionLancerExecute(Sender: TObject);
    procedure ActionExtinctionExecute(Sender: TObject);
    procedure ActionEteindreExecute(Sender: TObject);
    procedure ActionPauseExecute(Sender: TObject);
    procedure ActionReprendreExecute(Sender: TObject);
    procedure ActionResetExecute(Sender: TObject);
    procedure ActionSauverExecute(Sender: TObject);
    procedure ActionMiseEnVeilleExecute(Sender: TObject);
    procedure ActionPrendreLeControleExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ActionMachineProprieteExecute(Sender: TObject);
  private




    { D�clarations priv�es }
  public
    { D�clarations publiques }
    procedure AjouterParamtres(pParametres : String);
    procedure ActualiserMachines;
    procedure ActualiserMachinesActive;
    procedure ActualiserMachineImage;
    procedure ActualiserServeur;
    procedure ActualiserPopupMachinesActive;
    procedure VitualBoxShellExecute(Sender : TObject;Resultat : string);
    procedure VitualBoxEcrireMesssge(Sender : TObject;pMessage : string);
    procedure PopupMenuMachineLancer(Sender:TObject);
    procedure PopupMenuMachineExtinction(Sender:TObject);
    procedure PopupMenuMachineEteindre(Sender:TObject);
    procedure PopupMenuMachinePrendreLeControle(Sender:TObject);

  end;




var
  FormPrinc: TFormPrinc;


implementation

uses DateUtils, TypInfo, Math;

{$R *.dfm}



procedure TFormPrinc.ButtonPacourirClick(Sender: TObject);
var
  Dossier : String;
begin
    Dossier :=LabeledEditDossierTravail.Text;
  if(SelectDirectory('Dossier de travail','',Dossier)) then
    LabeledEditDossierTravail.Text := Dossier;

end;

procedure TFormPrinc.FormCreate(Sender: TObject);

begin
  Donnees.VirtualBox.OnEcrireMessage := VitualBoxEcrireMesssge;
  TabSheetShellCommande.TabVisible := Donnees.Parametres.ShellCommand.Activer;
(*
  if Donnees.Parametres.ShellCommand.Observateur
              and Donnees.Parametres.ShellCommand.Activer then
        Donnees.VirtualBox.OnShellExecute := VitualBoxShellExecute;
        *)
  LabelJournal.Visible := Donnees.Parametres.Serveur.Journaliser;
  ListBoxJournal.Visible := Donnees.Parametres.Serveur.Journaliser;
  Donnees.IdHTTPServer1.DefaultPort := Donnees.Parametres.Serveur.WebPort;
  Donnees.IdHTTPServer1.Active := Donnees.Parametres.Serveur.WebActif;
   {$IFNDEF DEBUG}
      TranslateAll(Self,LanguageFile);
   {$ENDIF}

  



end;

procedure TFormPrinc.ButtonExecuterClick(Sender: TObject);
begin
  Donnees.ShellCommande.DossierTravail := LabeledEditDossierTravail.Text;
  Donnees.ShellCommande.LigneCommande := ComboBoxLignesCommande.Text;
  Donnees.ShellCommande.Execute;
  MemoResultat.Lines.SetText(Donnees.ShellCommande.Resultat.GetText);
  AjouterParamtres(ComboBoxLignesCommande.Text);
end;


procedure TFormPrinc.AjouterParamtres(pParametres: String);
var
   i : Integer;
begin
  for i := 0 to ComboBoxLignesCommande.Items.Count - 1 do
    if SameText(pParametres,ComboBoxLignesCommande.Items[i])then
      exit;

  ComboBoxLignesCommande.Items.insert(0,pParametres);
end;






procedure TFormPrinc.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : integer;
begin
  TimerActualiser.Enabled := false;  
  Donnees.VirtualBox.DisconnectAllClients;
  Donnees.VirtualBox.Active := False;

  with Donnees.Parametres.ShellCommand do
  begin
    DossierDeTravail := LabeledEditDossierTravail.Text;
    Parametre := ComboBoxLignesCommande.Text;
    Historique.AddStrings(ComboBoxLignesCommande.Items);
  end;

  Donnees.EnregistrerParametres;
end;

procedure TFormPrinc.ActualiserMachines;
var
  i,j : integer;
  NodeMachine : TTreeNode;
  MenuItemMachine : TMenuItem;
  MenuItemAction : TMenuItem;
  Trouver : boolean;
  Suppression : boolean;
begin
  ActualiserServeur;
  Donnees.VirtualBox.Actualiser;

//*****  Actualisationn des machines*****///
//  TreeViewMachines.Items.Clear;
  repeat
    Suppression := false;
    for i := 0 to TreeViewMachines.Items.Count -1 do
    begin
      if Donnees.VirtualBox.Machines.ItemNom[TreeViewMachines.Items[i].Text] = nil then
      begin
        TreeViewMachines.Items[i].Delete;
        Suppression := true;
        break;
      end;
    end;
  until not Suppression;


  for i := 0 to Donnees.VirtualBox.Machines.Count -1 do
  begin
    Trouver := false;
    for j := 0 to TreeViewMachines.Items.Count -1 do
      if SameText(TreeViewMachines.Items[j].Text,Donnees.VirtualBox.Machines[i].Nom) then
      begin
        trouver := true;
        break;
      end;

    if Not Trouver then
    begin
      NodeMachine := TreeViewMachines.Items.AddChild(nil,Donnees.VirtualBox.Machines[i].Nom);
      NodeMachine.Data := Donnees.VirtualBox.Machines[i];
    end;
  end;


  //*** actualisation du menu ***//
  repeat
    Suppression := false;
    for i := 0 to PopupMachines.Count -1 do
    begin
      if Donnees.VirtualBox.Machines.ItemNom[PopupMachines.Items[i].Caption] = nil then
      begin
        PopupMachines.Delete(i);
        Suppression := true;
        break;
      end;
    end;
  until not Suppression;


  for i := 0 to Donnees.VirtualBox.Machines.Count -1 do
  begin
    Trouver := false;
    for j := 0 to PopupMachines.Count  -1 do
      if SameText(PopupMachines.Items[j].Caption,Donnees.VirtualBox.Machines[i].Nom) then
      begin
        trouver := true;
        break;

      end;

    if Not Trouver then
    begin
      MenuItemMachine := TMenuItem.Create(self);
      MenuItemMachine.Caption := Donnees.VirtualBox.Machines[i].Nom;
      MenuItemMachine.Tag := Integer(Donnees.VirtualBox.Machines[i]);

      MenuItemAction := TMenuItem.Create(self);
      MenuItemAction.Caption := 'Lancer';
      MenuItemAction.OnClick := PopupMenuMachineLancer;
      MenuItemAction.Tag := Integer(Donnees.VirtualBox.Machines[i]);
      MenuItemMachine.Add(MenuItemAction);

      MenuItemAction := TMenuItem.Create(self);
      MenuItemAction.Caption := 'Extinction';
      MenuItemAction.OnClick := PopupMenuMachineExtinction;
      MenuItemAction.Tag := Integer(Donnees.VirtualBox.Machines[i]);
      MenuItemMachine.Add(MenuItemAction);

      MenuItemAction := TMenuItem.Create(self);
      MenuItemAction.Caption := 'Eteindre';
      MenuItemAction.OnClick := PopupMenuMachineEteindre;
      MenuItemAction.Tag := Integer(Donnees.VirtualBox.Machines[i]);
      MenuItemMachine.Add(MenuItemAction);

      MenuItemAction := TMenuItem.Create(self);
      MenuItemAction.Caption := 'Prendre le contr�le';
      MenuItemAction.OnClick := PopupMenuMachinePrendreLeControle;
      MenuItemAction.Tag := Integer(Donnees.VirtualBox.Machines[i]);
      MenuItemMachine.Add(MenuItemAction);

      //PopupMenuTaches.Images := ImageMachine;
      PopupMachines.Add(MenuItemMachine);

    end;
  end;

end;

procedure TFormPrinc.ActionActualiserMachinesExecute(Sender: TObject);
begin
  ActualiserMachines;
  ActualiserMachinesActive;
  ActualiserMachineImage;
end;

procedure TFormPrinc.FormShow(Sender: TObject);
begin

  ActualiserMachines;
  ActualiserMachinesActive;
  ActualiserMachineImage;
  TimerActualiser.Enabled := true;
  ActualiserServeur;
    (*
  if  Donnees.Parametres.General.BarreDesTachesDemarrage then
  begin
     JvTrayIcon1.HideApplication;
   // JvTrayIcon1.Active := true;
//    JvTrayIcon1.ApplicationVisible := false;
//    Application.Minimize;
  end;
      *)
end;

procedure TFormPrinc.TreeViewMachinesClick(Sender: TObject);
var

  MachineSelect : boolean;
  Machine : TVBNVirtualBoxMachine;
begin
  MachineSelect := TreeViewMachines.Selected <> nil;
  ActionLancer.Enabled := MachineSelect;
  ActionExtinction.Enabled := MachineSelect;
  ActionEteindre.Enabled := MachineSelect;
  ActionPause.Enabled := MachineSelect;
  ActionReprendre.Enabled := MachineSelect;
  ActionReset.Enabled := MachineSelect;
  ActionSauver.Enabled := MachineSelect;
  ActionMiseEnVeille.Enabled := MachineSelect;

  if MachineSelect then
  begin
    Machine := TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data);
    LabelMachineNom.Caption := 'Machine : ' +Machine.Nom;
    LabelMachineUUID.Caption := 'UUID : '+Machine.UUID;
    ListBoxMachineInfo.Items.SetText(Pchar(Machine.LireInfo));
    ImageMachine.Picture := nil;
  end;
  ActualiserMachinesActive;
  ActualiserMachineImage;
end;

procedure TFormPrinc.VitualBoxShellExecute(Sender: TObject;
  Resultat: string);
begin
  MemoResultat.Lines.SetText(PChar(Resultat));
end;

procedure TFormPrinc.TimerActualiserTimer(Sender: TObject);
var
  Debut : Integer;
begin

  ActualiserMachines;
  ActualiserMachinesActive;
  ActualiserPopupMachinesActive;

  if JvTrayIcon1.ApplicationVisible then
    ActualiserMachineImage;



end;


procedure TFormPrinc.ActualiserMachinesActive;
var
  i : integer;
  Machine : TVBNVirtualBoxMachine;
begin
//  Donnees.VirtualBox.MachinesActives;
  for  i := 0 to TreeViewMachines.Items.Count -1 do
  begin
    if TVBNVirtualBoxMachine(TreeViewMachines.Items[i].Data).Active then
    begin
      TreeViewMachines.Items[i].ImageIndex := 1;
      TreeViewMachines.Items[i].StateIndex := 1;
      TreeViewMachines.Items[i].SelectedIndex := 1;
    end
    else
    begin
      TreeViewMachines.Items[i].ImageIndex := 0;
      TreeViewMachines.Items[i].StateIndex := 0;
      TreeViewMachines.Items[i].SelectedIndex := 0;
    end;
  end;

end;

procedure TFormPrinc.ActualiserPopupMachinesActive;
var
  i : integer;
  Machine : TVBNVirtualBoxMachine;
begin

  for  i := 0 to PopupMachines.Count -1 do
  begin
    if not TVBNVirtualBoxMachine(PopupMachines.Items[i].Tag).Active then
      PopupMachines.Items[i].ImageIndex := 0
    else
      PopupMachines.Items[i].ImageIndex := 1;

  end;

end;

procedure TFormPrinc.ActualiserMachineImage;
var
  Machine : TVBNVirtualBoxMachine;
  FichierPNG : string;
  ImagePNG : TPNGObject;
begin
  if TreeViewMachines.Selected <> nil then
  begin
    Machine := TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data);
    if Machine.Active then
    begin
      FichierPNG := Donnees.VirtualBox.DossierTravail+'\machine.png';
      Machine.ImprimeEcran(FichierPNG);
      if FileExists(FichierPNG) then
      begin
  //      ImagePNG := TPNGObject.Create;
//        ImagePNG.LoadFromFile(FichierPNG);

        ImageMachine.Picture.LoadFromFile(FichierPNG);
      end;
    end
    else
      ImageMachine.Picture := nil;


    LabelMachineState.Caption := 'Etat  : '+Machine.MachineState;
  end;

end;

procedure TFormPrinc.ActionActiverServeurExecute(Sender: TObject);
begin
  Donnees.VirtualBox.Active := not Donnees.VirtualBox.Active;
  ActualiserServeur;

end;

procedure TFormPrinc.VitualBoxEcrireMesssge(Sender: TObject;
  pMessage: string);
begin
  ListBoxJournal.Items.Insert(0,pMessage);
end;

procedure TFormPrinc.ActionActualiserImageExecute(Sender: TObject);
begin
  ActualiserMachineImage;
end;

procedure TFormPrinc.ActionParametresExecute(Sender: TObject);
begin
  With TFormVBParametres.Create(self) do
  begin
    if executer then
    begin
      Donnees.VirtualBox.DossierTravail := Donnees.Parametres.VirtualBox.DossierTravail;
      Donnees.VirtualBox.VBoxManage := Donnees.Parametres.VirtualBox.VBoxManage;
      Donnees.VirtualBox.ModeHeadless := Donnees.Parametres.VirtualBox.ModeHeadless;
      Donnees.VirtualBox.Utilisateur := Donnees.Parametres.Serveur.Utilisateur;
      Donnees.VirtualBox.MotDePasse := Donnees.Parametres.Serveur.MotDePasse;
      Donnees.VirtualBox.DefaultPort := Donnees.Parametres.Serveur.Port;
      Donnees.VirtualBox.Journaliser := Donnees.Parametres.Serveur.Journaliser;
      (*
      if Donnees.Parametres.ShellCommand.Observateur
              and Donnees.Parametres.ShellCommand.Activer then
            Donnees.VirtualBox.OnShellExecute := VitualBoxShellExecute;
            *)
      TabSheetShellCommande.TabVisible := Donnees.Parametres.ShellCommand.Activer;
      LabelJournal.Visible := Donnees.Parametres.Serveur.Journaliser;
      ListBoxJournal.Visible := Donnees.Parametres.Serveur.Journaliser;
      if (Donnees.Parametres.Serveur.WebActif xor Donnees.IdHTTPServer1.Active) or
        (Donnees.IdHTTPServer1.DefaultPort <> Donnees.Parametres.Serveur.WebPort) then
      begin
        Donnees.IdHTTPServer1.Active := false;
        Donnees.IdHTTPServer1.DefaultPort := Donnees.Parametres.Serveur.WebPort;
        Donnees.IdHTTPServer1.Active := Donnees.Parametres.Serveur.WebActif;

      end;

    end;
    free;
  end;
end;

procedure TFormPrinc.ActualiserServeur;
var
   Etat : string;
begin
  Donnees.ApplicationTitre := Application.Title +' V'+GetApplicationVersionString(Application);
  if Donnees.VirtualBox.Active then
  begin
    Caption := Donnees.ApplicationTitre+' - Serveur actif, port : ' +IntToStr(Donnees.VirtualBox.DefaultPort);
    Etat := Caption;
    ActionActiverServeur.Caption := 'D�sativer';
    JvTrayIcon1.IconIndex := 1;

  end
  else
  begin
    Caption := Donnees.ApplicationTitre+' - Serveur Desactiv�.';
    Etat := Caption;
    ActionActiverServeur.Caption := 'Activer';
    JvTrayIcon1.IconIndex := 0;
  end;



 // StatusBar1.SimpleText := Etat;
  JvTrayIcon1.Hint := Etat;

end;

procedure TFormPrinc.ActionQuitterExecute(Sender: TObject);
begin
  Close;
end;

procedure TFormPrinc.ActionAfficherExecute(Sender: TObject);
begin
  JvTrayIcon1.ShowApplication;
end;

procedure TFormPrinc.ActionLancerExecute(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).Lancer;

end;

procedure TFormPrinc.ActionExtinctionExecute(Sender: TObject);
begin
    if TreeViewMachines.Selected <> nil then
      TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).Extinction;
end;

procedure TFormPrinc.ActionEteindreExecute(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).Eteindre;
end;

procedure TFormPrinc.ActionPauseExecute(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).Pause;
end;

procedure TFormPrinc.ActionReprendreExecute(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).Reprendre;
end;

procedure TFormPrinc.ActionResetExecute(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).Reset;
end;

procedure TFormPrinc.ActionSauverExecute(Sender: TObject);
begin
    if TreeViewMachines.Selected <> nil then
      TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).Sauver;
end;

procedure TFormPrinc.ActionMiseEnVeilleExecute(Sender: TObject);
begin
    if TreeViewMachines.Selected <> nil then
      TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).MiseEnVeille;
end;




procedure TFormPrinc.PopupMenuMachineEteindre(Sender: TObject);
begin
  TVBNVirtualBoxMachine(TPopupMenu(Sender).Tag).Eteindre;
end;

procedure TFormPrinc.PopupMenuMachineExtinction(Sender: TObject);
begin
  TVBNVirtualBoxMachine(TPopupMenu(Sender).Tag).Extinction;
end;

procedure TFormPrinc.PopupMenuMachineLancer(Sender: TObject);
begin
   TVBNVirtualBoxMachine(TPopupMenu(Sender).Tag).Lancer;
end;

procedure TFormPrinc.PopupMenuMachinePrendreLeControle(Sender: TObject);
begin
  TVBNVirtualBoxMachine(TPopupMenu(Sender).Tag).PrendreLeControle;
end;

procedure TFormPrinc.ActionPrendreLeControleExecute(Sender: TObject);
begin
  if TreeViewMachines.Selected <> nil then
    TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data).PrendreLeControle;

end;

procedure TFormPrinc.FormActivate(Sender: TObject);
begin
  if  Donnees.Parametres.General.BarreDesTachesDemarrage then
  begin
     JvTrayIcon1.HideApplication;
   // JvTrayIcon1.Active := true;
//    JvTrayIcon1.ApplicationVisible := false;
//    Application.Minimize;
  end;

end;


procedure TFormPrinc.ActionMachineProprieteExecute(Sender: TObject);

begin
  if TreeViewMachines.Selected <> nil then
    With TFormVMachinePropriete.Create(self)do
    begin
      If executer(TVBNVirtualBoxMachine(TreeViewMachines.Selected.Data)) then
        Donnees.VirtualBox.EnregistrerParametresMachines(FichierMachinesINI);
    end;

end;

end.


